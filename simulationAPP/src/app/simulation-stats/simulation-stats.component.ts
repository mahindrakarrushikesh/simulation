import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { StatsService } from './stats.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;

@Component({
    selector: 'app-simulation-stats',
    templateUrl: './simulation-stats.component.html',
    styleUrls: ['./simulation-stats.component.css'],
    providers: [StatsService]
})
export class SimulationStatsComponent implements OnInit {
    plot_liver: any[];
    plot_plasma: any[];
    plot_vein: any[];
    Pkidney: any[];
    Pbrain: any[];
    Pbone: any[];
    Pplasma: any[];
    result: any;
    Pliver: any[];
    Pportvein: any[];
    id: number;
    stats: any;
    category: string;
    trialName: string;
    type: string;
    inputs: any;
    options: any;
    simulation_user: any;
    simulation_data: any;
    simulationData: any;
    inputId: number;
    totalSub: number = 0;
    vbePlot: any;

    constructor(private statsService: StatsService, private router: Router, route: ActivatedRoute) {
        if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
            this.router.navigateByUrl('login');
        } else {
            this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
            this.id = route.snapshot.params['id'];
        }
    }

    ngOnInit() {
        // option for graph
        this.options = {
            xaxes: [{
                axisLabel: 'Time (Hour)',
                tickColor: "#eee",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }],
            yaxes: [{
                axisLabel: 'Concentration (mg/L)',
                tickColor: "#eee",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }],
            grid: {
                borderColor: "#eee",
                borderWidth: 1
            }
        };

        this.inputs = {} as any;
        this.simulationData = {} as any;
        this.simulationData.population = {} as any;
        this.simulationData.drug = {} as any;
        this.vbePlot = {} as any;
        this.getData();
    }


    getData() {
        // get data to plat graph
        this.statsService.getGraphData(this.id)
            .subscribe(
                data => {
                    if (data.length > 0) {
                        var vm = this;
                        this.simulation_data = JSON.parse(data[0].data);
                        this.inputId = data[0].inputid;
                        this.getInputs(data[0].inputid);
                        if (this.simulation_data.plot_liver) {
                            this.type = 'fullpbpk';
                            setTimeout(function () {
                                // plot graph for case  fullpbpk
                                vm.ploatCase_fullpbpk();
                            }, 3000);
                        } else if (this.simulation_data.Pplasma) {
                            // plot graph for case minipbpk
                            this.type = 'minipbpk';
                            setTimeout(function () {
                                vm.ploatCase_minipbpk();
                            }, 3000);
                        } else {
                            this.type = 'vbe';
                            this.ploatCase_vbe();
                        }
                    }
                },
                error => {
                    this.errorHandler(error);
                });
    }
    // get inputsdata of simulation using id 
    getInputs(id) {
        this.statsService.getSimdata(id)
            .subscribe(data => {
                this.trialName = data.trial_name;
                this.inputs = data;
                this.simulationData = JSON.parse(data.simulation_data);
                if (this.simulationData && this.simulationData.trial_design && this.simulationData.trial_design.vbe && this.simulationData.trial_design.vbe.subject && this.simulationData.trial_design.vbe.subject.total_subject) {
                    this.totalSub = this.simulationData.trial_design.vbe.subject.total_subject;
                }
            },
                error => {
                    this.errorHandler(error);
                });
    }
    // play graph for case mini pbpk
    ploatCase_minipbpk() {
        this.vbePlot.Pplasma = $.plot($("#chart_1_2"),
            [{
                label: "Pplasma",
                data: this.simulation_data.Pplasma,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Liver 
        this.vbePlot.Pliver = $.plot($("#chart_1_3"),
            [{
                label: "Pliver",
                data: this.simulation_data.Pliver,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Vein 
        this.vbePlot.Pportvein = $.plot($("#chart_1_4"),
            [{
                label: "Pportvein",
                data: this.simulation_data.Pportvein,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );
        //Pkidney
        this.vbePlot.Pkidney = $.plot($("#chart_1_5"),
            [{
                label: "Pkidney",
                data: this.simulation_data.Pkidney,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Pbrain 
        this.vbePlot.Pbrain = $.plot($("#chart_1_6"),
            [{
                label: "Pbrain",
                data: this.simulation_data.Pbrain,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Pbone 
        this.vbePlot.Pbone = $.plot($("#chart_1_7"),
            [{
                label: "Pbone",
                data: this.simulation_data.Pbone,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        ///additional graph

        //padipose
        this.vbePlot.Padipose = $.plot($("#chart_1_8"),
            [{
                label: "Padipose",
                data: this.simulation_data.Padipose,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //partery
        this.vbePlot.Partery = $.plot($("#chart_1_9"),
            [{
                label: "Partery",
                data: this.simulation_data.Partery,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        //pgut
        this.vbePlot.Pgut = $.plot($("#chart_1_10"),
            [{
                label: "Pgut",
                data: this.simulation_data.Pgut,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //pheart
        this.vbePlot.Pheart = $.plot($("#chart_1_11"),
            [{
                label: "Pheart",
                data: this.simulation_data.Pheart,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        //plung
        this.vbePlot.Plung = $.plot($("#chart_1_12"),
            [{
                label: "Plung",
                data: this.simulation_data.Plung,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        //pmuscle
        this.vbePlot.Pmuscle = $.plot($("#chart_1_13"),
            [{
                label: "Pmuscle",
                data: this.simulation_data.Pmuscle,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //ppancreas
        this.vbePlot.Ppancreas = $.plot($("#chart_1_14"),
            [{
                label: "Ppancreas",
                data: this.simulation_data.Ppancreas,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        //pskin
        this.vbePlot.Pskin = $.plot($("#chart_1_15"),
            [{
                label: "Pskin",
                data: this.simulation_data.Pskin,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );


        //pspleen
        this.vbePlot.Pspleen = $.plot($("#chart_1_16"),
            [{
                label: "Pspleen",
                data: this.simulation_data.Pspleen,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );
    }
    // plot graph for case fullpbpk
    ploatCase_fullpbpk() {
        this.vbePlot.Plasma = $.plot($("#chart_1_2"),
            [{
                label: "Plasma",
                data: this.simulation_data.plot_plasma,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Plasma 
        this.vbePlot.Liver = $.plot($("#chart_1_3"),
            [{
                label: "Liver",
                data: this.simulation_data.plot_liver,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );

        //Plasma 
        this.vbePlot.Vein = $.plot($("#chart_1_4"),
            [{
                label: "Vein",
                data: this.simulation_data.plot_vein,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            }],
            this.options
        );



    }
    // plot graph for case vbe
    ploatCase_vbe() {
        let graphArray = [];
        for (let i = 0; i < this.simulation_data.plot.length; i++) {
            let obj = {
                label: i,
                data: this.simulation_data.plot[i].plot,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            };
            graphArray.push(obj);
        }


        // plat VBE graph
        var vm = this;
        setTimeout(function () {
            vm.vbePlot.VBE_OUTPUT_CHART = $.plot($("#chart_1_1"), graphArray, {
                xaxes: [{
                    axisLabel: 'Time (Hour)',
                    tickColor: "#eee",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                    axisLabelPadding: 5
                }],
                yaxes: [{
                    axisLabel: 'Concentration (mg/L)',
                    tickColor: "#eee",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                    axisLabelPadding: 5
                }],
                grid: {
                    borderColor: "#eee",
                    borderWidth: 1
                }
            });
        }, 2000);





    }
    // handle error which occure from api
    errorHandler(error) {
        if (error.error && error.error.message == 'Token has expired') {
            // token expired then clear local storage 
            localStorage.clear();
            this.router.navigateByUrl('login');
        }
    }
    // download pdf report 
    downloadReport() {
        var graph = this.generateImage();
        this.statsService.getReport(this.inputId, graph)
            .subscribe(data => {
                var file = new Blob([data], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL,'_blank');
            },
                error => {
                    this.errorHandler(error);
                });
    }
    // download csv report
    downloadCSV() {
        console.log(this.inputId);
        this.statsService.getCSV(this.inputId)
            .subscribe(data => {
                console.log(data);
                this.result = data;
                console.log(this.result);
                this.result[0].simulation_data = JSON.parse(this.result[0].simulation_data);
                this.result[0].data = JSON.parse(this.result[0].data);
                var csv = [];

                if (this.result[0].type == 'vbe_study') {
                    csv = [{
                        "Subject": this.result[0].simulation_data.trial_design.vbe.subject.total_subject,
                        "Sex": this.result[0].simulation_data.trial_design.vbe.subject.female,
                        "Age": this.result[0].simulation_data.trial_design.vbe.subject.age_max,
                        "BodyWeight": this.result[0].simulation_data.trial_design.vbe.subject.weight_max,
                        "Reference AUC": this.result[0].data.summary.RAUCMean,
                        "Reference Cmax": this.result[0].data.summary.RcmaxMean,
                        "Reference Tmax": this.result[0].data.summary.RtmaxMean,
                        "Test AUC": this.result[0].data.summary.RAUCMean,
                        "RAUC": this.result[0].data.summary.RAUCMean,
                        "Concentration-Time Profile": JSON.stringify(this.result[0].data.plot)
                    }];
                } else {
                    if (this.result[0].data.plot_vein) {
                        for (var i = 0; i < this.result[0].data.plot_plasma.length; i++) {
                            // create object of each row 
                            let row = {} as any;
                            row.Plasma_X = this.result[0].data.plot_plasma[i][0];
                            row.Plasma_Y = this.result[0].data.plot_plasma[i][1];

                            row.Liver_X = this.result[0].data.plot_liver[i][0];
                            row.Liver_Y = this.result[0].data.plot_liver[i][1];

                            row.vein_X = this.result[0].data.plot_vein[i][0];
                            row.vein_Y = this.result[0].data.plot_vein[i][1];
                            // push each object in to csv array
                            csv.push(row);
                        }

                    } else {

                        for (var i = 0; i < this.result[0].data.Pliver.length; i++) {
                            let row = {} as any;
                            row.Pplasma_X = this.result[0].data.Pplasma[i][0];
                            row.Pplasma_Y = this.result[0].data.Pplasma[i][1];

                            row.Pliver_X = this.result[0].data.Pliver[i][0];
                            row.Pliver_Y = this.result[0].data.Pliver[i][1];


                            row.Pportvein_X = this.result[0].data.Pportvein[i][0];
                            row.Pportvein_Y = this.result[0].data.Pportvein[i][1];


                            row.Pkidney_X = this.result[0].data.Pkidney[i][0];
                            row.Pkidney_Y = this.result[0].data.Pkidney[i][1];

                            row.Pbrain_X = this.result[0].data.Pbrain[i][0];
                            row.Pbrain_Y = this.result[0].data.Pbrain[i][1];


                            row.Pbone_X = this.result[0].data.Pbone[i][0];
                            row.Pbone_Y = this.result[0].data.Pbone[i][1];

                            row.Padipose_X = this.result[0].data.Padipose[i][0];
                            row.Padipose_Y = this.result[0].data.Padipose[i][1];

                            row.Partery_X = this.result[0].data.Partery[i][0];
                            row.Partery_Y = this.result[0].data.Partery[i][1];

                            row.Pgut_X = this.result[0].data.Pgut[i][0];
                            row.Pgut_Y = this.result[0].data.Pgut[i][1];


                            row.Pheart_X = this.result[0].data.Pheart[i][0];
                            row.Pheart_Y = this.result[0].data.Pheart[i][1];

                            row.Plung_X = this.result[0].data.Plung[i][0];
                            row.Plung_Y = this.result[0].data.Plung[i][1];

                            row.Pmuscle_X = this.result[0].data.Pmuscle[i][0];
                            row.Pmuscle_Y = this.result[0].data.Pmuscle[i][1];

                            row.Ppancreas_X = this.result[0].data.Ppancreas[i][0];
                            row.Ppancreas_Y = this.result[0].data.Ppancreas[i][1];

                            row.Pskin_X = this.result[0].data.Pskin[i][0];
                            row.Pskin_Y = this.result[0].data.Pskin[i][1];

                            row.Pspleen_X = this.result[0].data.Pspleen[i][0];
                            row.Pspleen_Y = this.result[0].data.Pspleen[i][1];

                            // push each object in to csv array
                            csv.push(row);
                        }
                    }
                }

                var csvData = this.ConvertToCSV(csv);
                var blob = new Blob([csvData], { type: 'text/csv' });
                var url = window.URL.createObjectURL(blob);
                window.open(url);

            },
                error => {
                    this.errorHandler(error);
                });
    }
    // convert json to csv
    ConvertToCSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = "";

        for (var index in objArray[0]) {
            //Now convert each value to string and comma-separated
            row += index + ',';
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }
            str += line + '\r\n';
        }
        return str;
    }
    // conver graph canvas to images 
    generateImage() {
        let array = [];
        for (var key in this.vbePlot) {
            let file = {} as any;
            this.vbePlot[key];
            file.name = key;
            var myCanvas = this.vbePlot[key].getCanvas();
            file.image = myCanvas.toDataURL();
            array.push(file);
        }
        return array
    }

}
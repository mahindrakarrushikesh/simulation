import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class SimulationInputService {

  // services
  private getPopulationUrl = environment.apiUrl + 'api/population';
  private getDrugUrl = environment.apiUrl + 'api/drug';
  private getTrialUrl = environment.apiUrl + 'api/trialDesign';
  private addNewSimulation = environment.apiUrl + 'api/addNewSimulation';
  private getsimdataUrl = environment.apiUrl + 'api/getsimdataUrl';
  private simulation_user: any;
  private headers: any;
  private userdetails: any;

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
      this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
    }
    this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
    if(this.userdetails.role == 'admin'){
      this.router.navigateByUrl('profile');
    }
    if(this.userdetails.status == 'pending'){
      this.router.navigateByUrl('varify');
    }
  }

  getPopulation() {
    return this.http.get<any>(this.getPopulationUrl, { headers: this.headers })
      .map(data => {
        console.log(data);
        return data;
      });
  }
  getSimdata(id) {
    return this.http.post<any>(this.getsimdataUrl, { id: id }, { headers: this.headers })
      .map(data => {
        console.log(data);
        return data;
      });
  }

  getDrug(type) {
    return this.http.post<any>(this.getDrugUrl, { type: type }, { headers: this.headers })
      .map(user => {
        return user;
      });
  }

  getTrialDesign(type) {
    return this.http.post<any>(this.getTrialUrl, { type: type }, { headers: this.headers })
      .map(user => {
        return user;
      });
  }


  saveSimulation(simulationData, type, trialname, timestamp) {
    return this.http.post<any>(this.addNewSimulation, { data: simulationData, studyType: type, trial_name: trialname, time : timestamp}, { headers: this.headers })
      .map(user => {
        return user;
      });
  }

}

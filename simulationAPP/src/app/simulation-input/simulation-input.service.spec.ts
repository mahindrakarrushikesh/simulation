import { TestBed, inject } from '@angular/core/testing';

import { SimulationInputService } from './simulation-input.service';

describe('SimulationInputService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SimulationInputService]
    });
  });

  it('should be created', inject([SimulationInputService], (service: SimulationInputService) => {
    expect(service).toBeTruthy();
  }));
});

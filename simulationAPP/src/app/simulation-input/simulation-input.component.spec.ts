import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulationInputComponent } from './simulation-input.component';

describe('SimulationInputComponent', () => {
  let component: SimulationInputComponent;
  let fixture: ComponentFixture<SimulationInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulationInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

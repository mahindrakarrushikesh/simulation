import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { SimulationInputService } from './simulation-input.service';
import { AddPopulationService } from '../add-population/add-population.service';
import { ToolTipService } from '../common/services/tool-tip.service';
import { AddTrialService } from '../add_trial/add-trial.service';
import { AddDrugService } from '../add_drug/add-drug.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-simulation-input',
  templateUrl: './simulation-input.component.html',
  styleUrls: ['./simulation-input.component.css'],
  providers: [SimulationInputService, ToastrService, ToolTipService, AddPopulationService, AddDrugService, AddTrialService]
})
export class SimulationInputComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  // attributes of UI 
  showSelected1: boolean;
  showSelected2: boolean;
  showSelected3: boolean;
  selectedpkpop: boolean;
  selectedpkdrug: boolean;
  selectedpktrial: boolean;
  validate: boolean;
  trialname: string;
  dlmDissolutionError: string;
  dlmDiTestValid: boolean;
  dlmParticleTestValid: boolean;
  dlmParticleError: string;
  select_absorption: string;
  select_distribution: string;
  select_metabolism: string;
  select_trial: number;
  select_drug: number;
  study_type: string;
  trialType: string;
  simtype: string;
  select_population: number;
  disableSelection: boolean = false;
  allTrial: any[];
  allData: any[];
  tooltippopulation: any;
  tooltipdrug: any;
  tooltippopulationdrug: any;
  tooltiptrialdesign: any;
  allPopulation: any = [];
  allDrug: any = [];
  populationdata: boolean;
  type: string;
  id: number;
  drug: any;
  tooltiptrial: any;
  simulationData: any;
  simulation_user: any;
  simulations: any;
  populationname: string;
  timestamp: any;
  testCaseError: string;
  loading: boolean = false;
  testValid: boolean = false;
  validTest: boolean;
  tooltipTrial: any;
  toolTipData: any;
  data: any;
  subject: any;

  constructor(private addTrialService: AddTrialService, private formBuilder: FormBuilder, private simulation: SimulationInputService, private toastrService: ToastrService, private router: Router, private route: ActivatedRoute, private tooltip: ToolTipService, private populationService: AddPopulationService, private drugService: AddDrugService) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
      this.id = this.route.snapshot.params['id'];
      if (this.id) {
        this.getSimdata();
      }
    }
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.type = params['type'];
      // retrive data on page load 
      this.getAllPopulation();
      this.getAllDrug();
      this.getAllTrialDesigns();
      this.gettootip();
    });

    this.type = this.route.snapshot.params['type'];
    if (this.type == "vbe_study") {
      this.simtype = "VBE Study";
    } else {
      this.simtype = "PK Study";
    }
    //init objects of Ui 
    this.allTrial = [];
    this.allPopulation = [];
    this.allDrug = [];
    this.timestamp = {} as any;
    this.data = {} as any;
    this.data.subject = {} as any;
    this.data.plan = {} as any;
    this.subject = {} as any;
    this.subject.blood_flow = {} as any;
    this.subject.tissue_volume = {} as any;

    this.simulationData = {} as any;
    this.simulations = {} as any;

    /* add drug */

    this.drug = {} as any;

    this.drug.physchem = {} as any;
    this.drug.absorption = {} as any;
    this.drug.absorption.dlm = {} as any;
    this.drug.absorption.dlm.dissolution = [];
    this.drug.absorption.dlm.particle = [];
    this.drug.absorption.adam = {} as any;
    this.drug.absorption.adam.dissolution = [];
    this.drug.absorption.first_order = {} as any;

    this.drug.metabolism = {} as any;
    this.drug.metabolism.mini_pbpk = {} as any;
    this.drug.metabolism.full_pbpk = {} as any;
    this.drug.metabolism.vbe = {} as any;



    this.drug.dissolution = {} as any;
    this.drug.dissolution.mini_pbpk = {} as any;
    this.drug.dissolution.full_pbpk = {} as any;
    this.drug.dissolution.vbe = {} as any;


    this.simulationData = {} as any;

    this.simulationData.population = {} as any;
    this.simulationData.drug = {} as any;


    this.tooltipTrial = {} as any;

    this.tooltipTrial.subject = {} as any;
    this.tooltipTrial.plan = {} as any;
    this.populationdata = false;
    this.selectedpkpop = true;
    this.selectedpkdrug = false;
    this.selectedpktrial = false;
    this.showSelected1 = true;
    this.showSelected2 = false;
    this.showSelected3 = false;
    this.validate = false;

    this.select_distribution = 'init';
    this.select_absorption = 'init';
    this.select_metabolism = 'init';

    /*For tooltips*/
    this.tooltippopulation = {} as any;
    this.tooltippopulation.blood_flow = {} as any;
    this.tooltippopulation.tissue_volumn = {} as any;

    this.tooltipdrug = {} as any;
    this.tooltipdrug.physchem = {} as any;
    this.tooltipdrug.absorption = {} as any;
    this.tooltipdrug.absorption.first_order = {} as any;
    this.tooltipdrug.absorption.dlm = {} as any;
    this.tooltipdrug.absorption.adam = {} as any;
    this.tooltipdrug.distribution = {} as any;
    this.tooltipdrug.distribution.mini_pbpk = {} as any;
    this.tooltipdrug.distribution.full_pbpk = {} as any;
    this.tooltipdrug.distribution.vbe = {} as any;
    this.tooltipdrug.metabolism = {} as any;
    this.tooltipdrug.metabolism.full_pbpk = {} as any;
    this.tooltipdrug.metabolism.mini_pbpk = {} as any;
    this.tooltipdrug.metabolism.vbe = {} as any;

    this.tooltipdrug.trial_design = {} as any;
    this.tooltipdrug.trial_design.vbe = {} as any;

    this.tooltiptrial = {} as any;
    this.getAllTrialDesigns();
    this.getAllPopulation();
    this.getAllDrug();
    this.gettootip();
  }

  getSimdata() {
    this.simulation.getSimdata(this.id)
      .subscribe(
        data => {
          this.disableSelection = true;
          this.simulations = data;
          this.simulationData = JSON.parse(data.simulation_data);
          //population
          this.subject = {} as any;
          this.subject.blood_flow = this.simulationData.population.subject.blood_flow;
          this.subject.tissue_volume = this.simulationData.population.subject.tissue_volume;
          //drug
          for (var prop in this.simulationData.drug.absorption) {
            this.select_absorption = prop;
          }
          for (var prop in this.simulationData.drug.metabolism) {
            this.select_distribution = prop;
          }
          this.drug = this.simulationData.drug;

          //trial
          if (this.select_distribution == 'mini_pbpk') {
            this.data = this.simulationData.trial_design.mini_pbpk;
          } else if (this.select_distribution == 'full_pbpk') {
            this.data = this.simulationData.trial_design.full_pbpk;
          } else {
            this.data = this.simulationData.trial_design.vbe;
          }
          if (this.data.plan.phase1_duration) {
            this.data.plan.selectd_dose = 'multiple';
          } else {
            this.data.plan.selectd_dose = 'single';
          }


        },
        error => {
          this.errorHandler(error);
        });
  }
// get tool tip data from json file 
  gettootip() {
    this.tooltip.getToolTopData()
      .subscribe(
        data => {
          this.tooltippopulation = data.population.subject;
          this.tooltipdrug = data.drug;
          this.toolTipData = data;
          if (this.data.plan.selectd_dose != 'multiple') {
            this.tooltipTrial = data.trial_design.full_pbpk;
          } else {
            this.tooltipTrial = data.trial_design.vbe;
          }

        },
        error => {
          this.errorHandler(error);
        });
  }
  // add Dissolution to Dissolution array
  addDissolution() {
    this.testValid = true;
    let array = [];
    array = this.drug.absorption.adam.dissolution;
    let solution = {} as any;
    solution.time = null;
    solution.test = null;
    solution.reference = null;
    array.push(solution);
    this.drug.absorption.adam.dissolution = array;
  }
  // remove Dissolution from Dissolution array 
  removeDissolution(index: number) {
    this.drug.absorption.adam.dissolution.splice(index, 1);
    this.validateTestCase();
  }
// step from show first step 
  firstStep() {
    this.showSelected1 = true;
    this.showSelected2 = false;
    this.showSelected3 = false;
    this.selectedpkpop = true;
    this.selectedpkdrug = false;
    this.selectedpktrial = false;
  }
// step from show second step
  secondStep() {
    this.showSelected1 = false;
    this.showSelected2 = true;
    this.showSelected3 = false;
    this.selectedpkpop = false;
    this.selectedpkdrug = true;
    this.selectedpktrial = false;
  }
// step from show third step
  thirdStep() {
    this.showSelected1 = false;
    this.showSelected2 = false;
    this.showSelected3 = true;
    this.selectedpkpop = false;
    this.selectedpkdrug = false;
    this.selectedpktrial = true;
  }

  distributionSelected(valule) {

  }

  //get simulation data 

  getAllTrialDesigns() {
    this.simulation.getTrialDesign(this.type)
      .subscribe(
        data => {
          this.allTrial = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
// get all drug for drug dropdown
  getAllDrug() {
    this.simulation.getDrug(this.type)
      .subscribe(
        data => {
          this.allDrug = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
// get all  populatuion for population drop down 
  getAllPopulation() {
    this.simulation.getPopulation()
      .subscribe(
        data => {
          this.allPopulation = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
// call when trial drop down selected 
  onTrailSelect(val: number) {
    if (!val) {
      return false;
    }
    for (let i = 0; this.allTrial.length; i++) {
      if (this.allTrial[i].id == this.select_trial) {
        this.timestamp.trial_timestamp = new Date(this.allTrial[i].created_on);
        this.data = JSON.parse(this.allTrial[i].trialData);
        break;
      }
    }
    if (this.data.plan.selectd_dose != 'multiple') {
      this.tooltipTrial = this.toolTipData.trial_design.full_pbpk;
    } else {
      this.tooltipTrial = this.toolTipData.trial_design.vbe;
    }
  }
// call when population drop down seleted 
  onPopulationSelect(val: number) {
    if (!val) {
      return false;
    }
    // load seleted populatuion in in UI 
    for (let i = 0; this.allPopulation.length; i++) {
      if (this.allPopulation[i].id == this.select_population) {
        this.timestamp.population_timestamp = new Date(this.allPopulation[i].created_on);
        this.subject.blood_flow = JSON.parse(this.allPopulation[i].blood_flow);
        this.subject.tissue_volume = JSON.parse(this.allPopulation[i].tissue_volumn);
        this.simulationData.population.subject = this.subject;
        this.simulationData.population.type = this.allPopulation[i].populationName;
        this.populationname = this.simulationData.population.type;
        break;
      }
    }
  }
// call when drug drop down seleted 
  onDrugSelect(val: number) {
    if (!val) {
      return false;
    }
    for (let i = 0; this.allDrug.length; i++) {
      if (this.allDrug[i].id == this.select_drug) {
        this.timestamp.drug_timestamp = new Date(this.allDrug[i].created_on);
        this.drug = JSON.parse(this.allDrug[i].drugdetails);
        for (let key in this.drug.distribution) {
          this.select_distribution = key;
        }
        for (let key in this.drug.absorption) {
          this.select_absorption = key;
        }
        break;
      }
    }
    if (this.drug.absorption && this.drug.absorption.adam && this.drug.absorption.adam.dissolution) {
      this.validateTestCase();
    } else {
      this.testValid = false;
      this.dlmParticleTestValid = false;
      this.dlmDiTestValid = false;
    }
  }
// submit simulation to Ui 
  submitSimulation() {
    this.loading = true;
    // preprocess simulation input  
    this.data.subject.population = this.populationname;
    this.simulationData.drug = this.drug;
    this.simulationData.trial_design = {} as any;
    var trialKey = '';
    for (let key in this.simulationData.drug.distribution) {
      trialKey = key;
    }
    //filter trial design
    this.trialname = this.data.trialName;
    delete this.data.trialName;
    delete this.data.plan.selectd_dose;

    if (trialKey == 'mini_pbpk') {
      this.simulationData.trial_design.mini_pbpk = this.data;
    } else if (trialKey == 'full_pbpk') {
      this.simulationData.trial_design.full_pbpk = this.data;
    } else {
      this.simulationData.trial_design.vbe = this.data;
    }
    // save simulation data 
    this.simulation.saveSimulation(this.simulationData, this.type, this.trialname, this.timestamp)
      .subscribe(
        data => {
          console.log(data);
          this.loading = false;
          if (data.outputId) {
            this.toastrService.success('success');
            this.router.navigate(['/simulation_stats', data.outputId]);
          } else if (data.error) {//for system error
            this.toastrService.error(data.error);
          } else {
            this.toastrService.error(data);
          }
          this.simulationData = {} as any;
        },
        error => {
          this.errorHandler(error);
        });

  }
// update population
  updatePopulation() {
    let populationData = { 'id': this.select_population, 'populationName': this.simulationData.population.type, 'blood_flow': this.subject.blood_flow, 'tissue_volumn': this.subject.tissue_volume };
    this.populationService.updatePolulation(populationData)
      .subscribe(
        data => {
          this.loading = false;
          if (data.result == 'Population updated successfully') {
            this.toastrService.success('Population updated successfully');
          } else {
            this.toastrService.error(data);
          }
        },
        error => {
          this.loading = false;
          this.errorHandler(error);
        });
  }
//update drug data 
  updateDrug() {
// preprocess drug data 
    if (this.drug.distribution.full_pbpk) {
      this.study_type = "pk_study";
    } else if (this.drug.metabolism.vbe) {
      this.study_type = "vbe_study";
    } else if (this.drug.distribution.mini_pbpk) {
      this.study_type = "pk_study";
    }
    this.drugService.updateDrug(this.select_drug, this.drug, this.study_type)
      .subscribe(
        data => {
          this.loading = false;
          this.toastrService.success('Drug updated successfully');
        },
        error => {
          this.loading = false;
          this.errorHandler(error);
        });
  }
// update trial data 
  updateTrial() {
    // preprocess trial data bvefore save 
    if (this.data.plan.selectd_dose == 'single') {
      this.trialType = "pk_study";
    } else {
      this.trialType = "vbe_study";
    }
    this.addTrialService.updateTrial(this.select_trial, this.data.trialName, this.data, this.trialType)
      .subscribe(
        data => {
          this.loading = false;
          this.toastrService.success('Trial data updated successfully');
        },
        error => {
          this.loading = false;
          this.errorHandler(error);
        });
  }
// validate trial data 
  validateTestCase() {
    this.testCaseError = "";
    this.testValid = true;
    for (let i = 0; i < this.drug.absorption.adam.dissolution.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.adam.dissolution[i - 1].time > this.drug.absorption.adam.dissolution[i].time) {
          this.testCaseError = "time value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i - 1].test > this.drug.absorption.adam.dissolution[i].test) {
          this.testCaseError = "test value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i - 1].reference > this.drug.absorption.adam.dissolution[i].reference) {
          this.testCaseError = "reference value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].test > 100) {
          this.testCaseError = "test should be less than 100";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].reference > 100) {
          this.testCaseError = "reference should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.adam.dissolution[i].test > 100) {
          this.testCaseError = "test should be less than 100";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].reference > 100) {
          this.testCaseError = "reference should be less than 100";
          return false;
        }
      }
    }
    this.testValid = false;
  }

// handle error which occure from api
  errorHandler(error) {
    if (error.error && error.error.message == 'Token has expired') {
      // token expired then clear local storage 
      this.toastrService.error(error.error.message);
      localStorage.clear();
      this.router.navigateByUrl('login');
    }
  }

// validate Dissolution test cases 
  validateDissolution() {
    this.dlmDissolutionError = "";
    this.dlmDiTestValid = true;
    for (let i = 0; i < this.drug.absorption.dlm.dissolution.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.dlm.dissolution[i - 1].release_time > this.drug.absorption.dlm.dissolution[i].release_time) {
          this.dlmDissolutionError = "release time value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.dissolution[i - 1].release_fraction > this.drug.absorption.dlm.dissolution[i].release_fraction) {
          this.dlmDissolutionError = "release fraction value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.dissolution[i].release_fraction > 100) {
          this.dlmDissolutionError = "release fraction should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.dlm.dissolution[i].release_fraction > 100) {
          this.dlmDissolutionError = "release fraction should be less than 100";
          return false;
        }
      }
    }
    this.dlmDiTestValid = false;
  }


// validate particle test cases 
  validateParticle() {
    this.dlmParticleError = "";
    this.dlmParticleTestValid = true;
    for (let i = 0; i < this.drug.absorption.dlm.particle.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.dlm.particle[i - 1].particle_radius > this.drug.absorption.dlm.particle[i].particle_radius) {
          this.dlmParticleError = "Particle radius value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.particle[i - 1].particle_fraction > this.drug.absorption.dlm.particle[i].particle_fraction) {
          this.dlmParticleError = "Particle fraction value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.particle[i].particle_fraction > 100) {
          this.dlmParticleError = "release fraction should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.dlm.particle[i].particle_fraction > 100) {
          this.dlmParticleError = "Particle fraction should be less than 100";
          return false;
        }
      }
    }
    this.dlmParticleTestValid = false;
  }


// add Dlm Dissolution to Dissolution array
  addDlmDissolution() {
    let array = [];
    array = this.drug.absorption.dlm.dissolution;
    let solution = {} as any;
    solution.release_time = null;
    solution.release_fraction = null;
    array.push(solution);
    this.drug.absorption.dlm.dissolution = array;
  }
  // remove dissolution from dissolution array 
  removeDlmDissolution(index: number) {
    this.drug.absorption.dlm.dissolution.splice(index, 1);
  }
  // add Particle Dissolution to Particle Dissolution  array
  addParticleDissolution() {
    let array = [];
    array = this.drug.absorption.dlm.particle;
    let solution = {} as any;
    solution.particle_radius = null;
    solution.particle_fraction = null;
    array.push(solution);
    this.drug.absorption.dlm.particle = array;
  }
  removeParticleDissolution(index: number) {
    this.drug.absorption.dlm.particle.splice(index, 1);
  }
}

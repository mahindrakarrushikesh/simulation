import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTrialDesignComponent } from './view-trial-design.component';

describe('ViewTrialDesignComponent', () => {
  let component: ViewTrialDesignComponent;
  let fixture: ComponentFixture<ViewTrialDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTrialDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTrialDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

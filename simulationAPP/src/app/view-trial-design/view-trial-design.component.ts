import { Component, OnInit } from '@angular/core';
import { TrialDesignService } from './trial-design.service';
import { Router } from '@angular/router';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-view-trial-design',
  templateUrl: './view-trial-design.component.html',
  styleUrls: ['./view-trial-design.component.css'],
  providers: [TrialDesignService, ToastrService]
})
export class ViewTrialDesignComponent implements OnInit {
    allTrial: any = [];
	private simulation_user:any;
    constructor(private trialDesignService: TrialDesignService,private router: Router, private toastrService: ToastrService,) {
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		}else {
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
		}
	}

  ngOnInit() {
	  this.getAllTrialDesigns();
  }
  getAllTrialDesigns() {
    this.trialDesignService.getTrialDesign()
      .subscribe(
        data => {
          console.log(data);
          this.allTrial = data;
        },
        error => {
          if (error.error && error.error.message == 'Token has expired') {
            localStorage.clear();
            this.router.navigateByUrl('login');
          }
        });
  }
  deleteTrial(id){
	  console.log("===id==");
	  console.log(id);
	    this.trialDesignService.deleteTrial(id)
      .subscribe(
        data => {
			console.log(data);
			if(data.res=='ok'){
				  this.toastrService.success('Drug deleted successfully');
				   this.getAllTrialDesigns();
			}
	          },
        error => {
          console.log(error);
        });
  }
  viewTrial(id){
    this.router.navigate(['add_trial', id]);
  }

}

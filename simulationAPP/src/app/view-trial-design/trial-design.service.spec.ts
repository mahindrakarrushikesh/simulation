import { TestBed, inject } from '@angular/core/testing';

import { TrialDesignService } from './trial-design.service';

describe('TrialDesignService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrialDesignService]
    });
  });

  it('should be created', inject([TrialDesignService], (service: TrialDesignService) => {
    expect(service).toBeTruthy();
  }));
});

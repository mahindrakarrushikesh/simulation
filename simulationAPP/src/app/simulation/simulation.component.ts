import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { SimulationService } from './simulation.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.css'],
  providers: [SimulationService]
})
export class SimulationComponent implements OnInit {
  allSimulation: any = [];
  simulation_data: any = [];
  simulation_user: any;
  constructor(private simulationService: SimulationService, private router: Router) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
    }
  }

  ngOnInit() {
    // get all simulation data 
    this.getAllSimulation();
  }
// get all simulation data 
  getAllSimulation() {
    this.simulationService.getSimulation()
      .subscribe(
        data => {
         // process simulation data 
          data.forEach(element => {
            if (element.simulation_data) {
              if (typeof element.simulation_data) {
              //  convert json string to json object
                let input = JSON.parse(element.simulation_data);
                if (input.population && input.drug && element) {
                  element.input = input.population.type + "-" + input.drug.name + "-" + element.trial_name;
                }
              }
            }
          });
          this.allSimulation = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
  viewSimulation(id) {
    this.router.navigate(['simulation_stats', id]);
  }
  viewInput(type, id) {
    this.router.navigate(['input', type, id]);
  }

// handle error which occure from api
  errorHandler(error) {
    if (error.error && error.error.message == 'Token has expired') {
      // token expired then clear local storage 
      localStorage.clear();
      this.router.navigateByUrl('login');
    }
  }



}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Injectable()
export class AddTrialService {

	private addTrialURL = environment.apiUrl + 'api/add_trial';
	private updateTrialURL = environment.apiUrl + 'api/update_trial';
	private getTrialurl = environment.apiUrl + 'api/get_trial';
	private simulation_user: any;
	private headers: any;
	private userdetails: any;
	constructor(private http: HttpClient, private router: Router) {

		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		} else {
			// get token from local storage and set it in header 
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
			this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
			this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
		}
		// check user role and redurect user to that view 
		this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
		if(this.userdetails.role == 'admin'){
            this.router.navigateByUrl('profile');
		  }
		  // if user is not varified then send user toi varify page
		if(this.userdetails.status == 'pending'){
            this.router.navigateByUrl('varify');
          }

	}

// save trial design to database 
	saveTrial(name: string, trialData: any, type: string) {
		return this.http.post<any>(this.addTrialURL, { trialName: name, trial: trialData, trialType: type }, { headers: this.headers })
			.map(data => {
				return data;
			});
	}
	// update trial to databse 
	updateTrial(id: number, name: string, trialData: any, type: string) {
		return this.http.post<any>(this.updateTrialURL, { id: id, trialName: name, trial: trialData, trialType: type }, { headers: this.headers })
			.map(data => {
				return data;
			});
	}
// get trial details from database 
	getTrial(id: number) {
		return this.http.post<any>(this.getTrialurl, { id: id }, { headers: this.headers })
			.map(data => {
				return data;
			});
	}
}

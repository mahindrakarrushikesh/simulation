import { TestBed, inject } from '@angular/core/testing';

import { AddTrialService } from './add-trial.service';

describe('AddTrialService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddTrialService]
    });
  });

  it('should be created', inject([AddTrialService], (service: AddTrialService) => {
    expect(service).toBeTruthy();
  }));
});

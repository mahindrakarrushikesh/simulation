import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
import { AddTrialService } from './add-trial.service';
import { ToolTipService } from '../common/services/tool-tip.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-add_trial',
	templateUrl: './add_trial.component.html',
	styleUrls: ['./add_trial.component.css'],
	providers: [AddTrialService, ToastrService, ToolTipService]
})

export class AddTrialComponent implements OnInit {
	@ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
	id: number;
	subject: any;
	plan: any;
	trialName: string;
	trial: any;
	data: any;
	simulation_user: any;
	tooltipTrial: any;
	trialType: any;
	toolTipData: any;
	loading: boolean = false;
	constructor(private addTrialService: AddTrialService, private toastrService: ToastrService, private router: Router, route: ActivatedRoute, private tooltip: ToolTipService) {
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		} else {
			// get user data from local storage 
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
			this.id = route.snapshot.params['id'];
			if (this.id) {
				// get tooltip data for drug object from tooltip.json file 
				this.getTrial();
			}
		}
	}
	ngOnInit() {
		// init object for UI
		this.trial = {} as any;
		this.data = {} as any;
		this.data.subject = {} as any;
		this.data.plan = {} as any;



		// init objects for tooltips 
		this.tooltipTrial = {} as any;

		this.tooltipTrial.subject = {} as any;
		this.tooltipTrial.plan = {} as any;
		this.gettootip();

	}
	// get tooltip data for drug object from tooltip.json file 
	public gettootip() {
		this.tooltip.getToolTopData()
			.subscribe(
				data => {
					this.toolTipData = data;
					if (this.data.plan.selectd_dose != 'multiple') {
						this.tooltipTrial = data.trial_design.full_pbpk;
					} else {
						this.tooltipTrial = data.trial_design.vbe;
					}
				},
				error => {

				});
	}
	// retrive trial details from database using trial id for save or view  
	getTrial() {
		this.addTrialService.getTrial(this.id)
			.subscribe(
				data => {
					this.data = JSON.parse(data.trialData);
					if (this.data.plan.phase2_duration) {
						this.tooltipTrial = this.toolTipData.trial_design.vbe;
					} else {
						this.tooltipTrial = this.toolTipData.trial_design.full_pbpk;
					}
				},
				error => {
					if (error.error && error.error.message == 'Token has expired') {
						localStorage.clear();
						this.router.navigateByUrl('login');
					}
				});
	}
// save trial in database 
	submitForm() {
		this.loading = true;
		if (this.data.plan.selectd_dose == 'single') {
			this.trialType = "pk_study";
		} else {
			this.trialType = "vbe_study";
		}
		if (this.id) {
			// update trial 
			this.addTrialService.updateTrial(this.id, this.data.trialName, this.data, this.trialType)
				.subscribe(
					data => {
						this.loading = false;
						this.toastrService.success('Trial data updated successfully');
					},
					error => {
						this.loading = false;
						this.errorHandler(error);
					});

		} else {
//save trial 
			this.addTrialService.saveTrial(this.data.trialName, this.data, this.trialType)
				.subscribe(
					data => {
						this.loading = false;
						this.toastrService.success('Trial data added successfully');
					},
					error => {
						this.loading = false;
						this.errorHandler(error);
					});

		}
	}
// call when dose drop down selected 
	onDoseSelect() {
		if (this.data.plan.selectd_dose != 'multiple') {
			this.tooltipTrial = this.toolTipData.trial_design.full_pbpk;
		} else {
			this.tooltipTrial = this.toolTipData.trial_design.vbe;
		}
	}
  // handle error which occure from api
	errorHandler(error) {
		if (error.error && error.error.message == 'Token has expired') {
			// token expired then clear local storage 
			this.toastrService.error(error.error.message);
			localStorage.clear();
			this.router.navigateByUrl('login');
		}
	}

}

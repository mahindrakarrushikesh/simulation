import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes, ParamMap } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile/profile.component';
import { OtpComponent } from './otp/otp.component';
import { BillingInfoComponent } from './billing-info/billing-info.component';
import { SimulationComponent } from './simulation/simulation.component';
import { SimulationInputComponent } from './simulation-input/simulation-input.component';
import { SimulationStatsComponent } from './simulation-stats/simulation-stats.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HeaderComponent } from './componants/header/header.component';
import { SidebarComponent } from './componants/sidebar/sidebar.component';
import { FooterComponent } from './componants/footer/footer.component';
import { BreadcrumbComponent } from './componants/breadcrumbs/breadcrumbs.component';
import { PopulationComponent } from './population/population.component';
import { AddPopulationComponent } from './add-population/add-population.component';
import { AddDrugComponent } from './add_drug/add_drug.component';
import { AddTrialComponent } from './add_trial/add_trial.component';
import { ViewTrialDesignComponent } from './view-trial-design/view-trial-design.component';
import { ViewDrugComponent } from './view-drug/view-drug.component';
import { LoadingModule } from 'ngx-loading';
import { AdminComponent } from './admin/admin.component';
//import { AppRoutingModule } from './/app-routing.module'


/* Routes */
const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgot_password', component: ForgotPasswordComponent },
  { path: '', component: SignupComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'billing_info', component: BillingInfoComponent },
  { path: 'simulation', component: SimulationComponent },
  { path: 'input/:type', component: SimulationInputComponent },
  { path: 'input/:type/:id', component: SimulationInputComponent },
  {path: 'simulation_stats/:id', component: SimulationStatsComponent }, 
  {path: 'simulation_stats', component: SimulationStatsComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'reset_password', component: ResetPasswordComponent },
  { path: 'reset_password/:otp/:email', component: ResetPasswordComponent },
  { path: 'population', component: PopulationComponent },
  { path: 'add_population', component: AddPopulationComponent },
  { path: 'add_population/:id', component: AddPopulationComponent },
  { path: 'add_drug', component: AddDrugComponent },
  { path: 'add_drug/:id', component: AddDrugComponent },
  { path: 'add_trial', component: AddTrialComponent },
  { path: 'add_trial/:id', component: AddTrialComponent },
  { path: 'view_trial', component: ViewTrialDesignComponent },
  { path: 'view_drug', component: ViewDrugComponent },
  { path: 'varify', component: OtpComponent },
  { path: 'admin', component: AdminComponent }
];

@NgModule({
	
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent,
    ProfileComponent,
    OtpComponent,
    BillingInfoComponent,
    SimulationComponent,
    SimulationInputComponent,
    SimulationStatsComponent,
    ResetPasswordComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    BreadcrumbComponent,
    PopulationComponent,
    AddPopulationComponent,
    AddDrugComponent,
    AddTrialComponent,
    ViewTrialDesignComponent,
    ViewDrugComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,ReactiveFormsModule,HttpModule,HttpClientModule,
	BrowserAnimationsModule,
    ToastrModule.forRoot(),	
	LoadingModule, 
	//AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
 
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AdminService {
  private getAllUsersUrl = environment.apiUrl + 'api/admin/getUsers';
  private getUserUrl = environment.apiUrl + 'api/admin/getUser';
  private deleteUserUrl = environment.apiUrl + 'api/admin/deleteUser';
  private updateUserUrl = environment.apiUrl + 'api/admin/updateUser';
  private addUserUrl = environment.apiUrl + 'api/admin/addUser';

  private simulation_user: any;
  private headers: any;
  private userdetails: any;

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
      this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
      this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
    }
    this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
    if (this.userdetails.status == 'pending') {
      this.router.navigateByUrl('varify');
    }

    if (this.userdetails.role == 'user') {
      this.router.navigateByUrl('profile');
    }


  }
//  get all user data 
  getAllUsers() {
    return this.http.get<any>(this.getAllUsersUrl, { headers: this.headers })
      .map(data => {
        return data;
      });
  }
// get user deatisl using user id 
  getUserDetails(id) {
    return this.http.post<any>(this.getUserUrl, { id: id }, { headers: this.headers })
      .map(data => {
        return data;
      });
  }
// delete user 
  deleteUser(id) {
    return this.http.post<any>(this.deleteUserUrl, { id: id }, { headers: this.headers })
      .map(data => {
        return data;
      });
  }
// update user 
  updateUser(user) {
    console.log(user);
    console.log("updating data");
    return this.http.post<any>(this.updateUserUrl, { user: user }, { headers: this.headers })
      .map(data => {
        return data;
      });
  }
// add new user to database
  addUser(user) {
    return this.http.post<any>(this.addUserUrl, { user: user }, { headers: this.headers })
      .map(data => {
        return data;
      });
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
import { AdminService } from './admin.service';
import { ToolTipService } from '../common/services/tool-tip.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [AdminService]
})
export class AdminComponent implements OnInit {
  users: any[];
  user: any;
  edit: boolean = false;
  constructor(private admin: AdminService, private router: Router,) { }

  ngOnInit() {
    this.user = {} as any;
    this.loadUsers();
  }
  // get all users
  loadUsers() {
    this.admin.getAllUsers()
      .subscribe(
        data => {
          this.users = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
  // show add user form 
  createUser() {
    this.user = {} as any;
    $("#user-model").modal('show');
  }
  // show edit user form and get user data
  editUser(id) {
    this.edit = true;
    this.admin.getUserDetails(id)
      .subscribe(
        data => {
          this.edit = true;
          $("#user-model").modal('show');
          this.user = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
  // delete exsting user using user id
  deleteUser(id) {
    this.admin.deleteUser(id)
      .subscribe(
        data => {
          this.loadUsers();
        },
        error => {
          this.errorHandler(error);
        });
  }
  // add new user to database 
  addUser(form) {
    if (this.user.id) {
      this.updateUser();
    } else {
      this.edit = false;
      console.log(this.user);
      this.admin.addUser(this.user)
        .subscribe(
          data => {
            form.resetForm();
            $("#user-model").modal('hide');
            this.loadUsers();
          },
          error => {
            this.errorHandler(error);
          });
    }
  }
// update user details 
  updateUser() {
    this.edit = false;
    console.log(this.user);
    this.admin.updateUser(this.user)
      .subscribe(
        data => {
          $("#user-model").modal('hide');
          this.loadUsers();
        },
        error => {
          this.errorHandler(error);
        });
  }
// handle error which occure from api
  errorHandler(error){
		if (error.error && error.error.message == 'Token has expired') {
       // token expired then clear local storage 
			localStorage.clear();
			this.router.navigateByUrl('login');
		}
	}
}

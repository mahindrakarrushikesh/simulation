import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileService } from './profile.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css'],
	providers: [ProfileService, ToastrService]
})
export class ProfileComponent implements OnInit {
	passworddata: any;
	profiledata: any;
	billingdata: any;
	private simulation_user: any;
	public userdetails: any;
	public pwd: any = '';
	constructor(private profile: ProfileService, private router: Router, private toastrService: ToastrService) {
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		} else {
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
			this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
			// load user data 
			this.getUserInfo();
		}
	}

	ngOnInit() {
		// init object for UI 
		this.passworddata = {} as any;
		this.profiledata = {} as any;
		this.billingdata = {} as any ;
	}

	getUserInfo() {
		// get user detaiols from api 
		this.profile.getmyInfo().subscribe(
			data => {
				let info = data[0];
				this.profiledata = {
					firstname: info.firstname,
					lastname: info.lastname,
					mobile: info.mobile,
					about: info.about,
					email: info.email,
					occupation: info.occupation
				};
				this.pwd = data[0].password;
				this.billingdata = {
					houseno: info.houseno,
					street: info.street,
					landmark: info.landmark,
					city: info.city,
					state: info.state,
					country: info.country,
					zipcode: info.zipcode
				};

			},
			error => {
          this.errorHandler(error);
			});
	}
 // handle error which occure from api
	errorHandler(error){
		if (error.error && error.error.message == 'Token has expired') {
			// token expired then clear local storage 
			this.toastrService.error(error.error.message);
			localStorage.clear();
			this.router.navigateByUrl('login');
		}
	}
	// submit profile details to save 
	submitForm(type, formname) {
    if (type == "profile") {
			this.profile.addProfileInfo(this.profiledata)
				.subscribe(
					data => {
						this.toastrService.success('Profile Updated Successfully');
					},
					error => {
						this.errorHandler(error);
					});

		} else if (type == "password") {
			// update password 
			let userpwd = { newpassword: this.passworddata.newpassword };
			if (this.pwd == this.passworddata.currentpassword) {
				this.profile.sendPasswordInfo(userpwd)
					.subscribe(
						data => {
							this.toastrService.success('Password Updated Successfully');
							formname.resetForm();
						},
						error => {
							this.errorHandler(error);
						});
			} else {
				
				this.toastrService.error('Current password not match');
			}
		} else if (type == "billing") {
			// update billing information to database 
			this.profile.addUseraddress(this.billingdata)
				.subscribe(
					data => {
						this.toastrService.success('Address Updated Successfully');
					},
					error => {
						this.errorHandler(error);
					});

		}
		// get user information after save user details 
		this.getUserInfo();
	}
}

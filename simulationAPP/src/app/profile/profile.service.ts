import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class ProfileService {
  // services
  private profileURL = environment.apiUrl + 'api/addUser';
  private addUseraddressURL = environment.apiUrl + 'api/addUseraddress';
  private passwordURL = environment.apiUrl + 'api/setPasswordInfo';
  private myInfo = environment.apiUrl + 'api/myInfo';
  private checkheaderurl = environment.apiUrl + 'api/checkheader';
  private simulation_user:any;
  private headers;
  private userdetails: any;
// dependacy injection 
  constructor(private http: HttpClient,private router: Router){
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		}else {
			this.simulation_user = 'Bearer '+JSON.parse(localStorage.getItem('simulation_user'));
			this.headers = new HttpHeaders({'Authorization': this.simulation_user});
        }
        this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
        if(this.userdetails.status == 'pending'){
            this.router.navigateByUrl('varify');
        }
  }
// API to get login user information 
 getmyInfo(){
     return this.http.get<any>(this.myInfo, { headers: this.headers })
      .map(user => {
            return user;
    });
 }
// update user profile information 
     addProfileInfo(profiledata){
	   return this.http.post<any>(this.profileURL, {profiledata : profiledata},{ headers: this.headers })
        .map(user => {
            return user;
        });
     } 
     
     // update address details 
	 addUseraddress(billingdata){
	   return this.http.post<any>(this.addUseraddressURL, {billingdata : billingdata},{ headers: this.headers })
        .map(user => {
            return user;
        });
     }
     // update password of user 
	 sendPasswordInfo(passworddata){
	   return this.http.post<any>(this.passwordURL, {passworddata : passworddata},{ headers: this.headers })
        .map(user => {
            return user;
        });
     }
     
}
 
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class DrugService {

    private getDrugUrl = environment.apiUrl + 'api/drug';
    private deletedrugid = environment.apiUrl + 'api/deletedrug';
	private simulation_user:any;
  private headers:any;
  private userdetails: any;
  constructor(private http: HttpClient,private router: Router) {
	if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
		this.router.navigateByUrl('login');
	}else {
		this.simulation_user = 'Bearer '+JSON.parse(localStorage.getItem('simulation_user'));
		this.headers = new HttpHeaders({'Authorization': this.simulation_user});
  }
  this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
  if(this.userdetails.role == 'admin'){
    this.router.navigateByUrl('profile');
  }
  if(this.userdetails.status == 'pending'){
    this.router.navigateByUrl('varify');
  }
  }


  getDrug() {
    return this.http.get<any>(this.getDrugUrl,{ headers: this.headers })
      .map(drug => {
        return drug;
      });
  }
  deleteDrug(id){
	return this.http.post<any>(this.deletedrugid,{id:id},{ headers: this.headers })
      .map(drug => {
        return drug;
      });  
  }


}

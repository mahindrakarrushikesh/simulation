import { Component, OnInit } from '@angular/core';
import { DrugService } from './drug.service';
import { Router } from '@angular/router';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-view-drug',
  templateUrl: './view-drug.component.html',
  styleUrls: ['./view-drug.component.css'],
   providers: [DrugService,ToastrService]
})
export class ViewDrugComponent implements OnInit {
  allDrug: any = [];
  simulation_user:any;
  
  constructor(private drugService: DrugService,private router: Router,private toastrService: ToastrService) {  
	if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
		this.router.navigateByUrl('login');
	}else {
    this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
    if(this.simulation_user.status == 'pending'){
      this.router.navigateByUrl('varify');
    }
	}
  }

  ngOnInit() {
	  this.getAllDrug();
  }

  getAllDrug() {
    this.drugService.getDrug()
      .subscribe(
        data => {
          this.allDrug = data;
        },
        error => {
          if (error.error && error.error.message == 'Token has expired') {
            localStorage.clear();
            this.router.navigateByUrl('login');
          }
        });
  }

deleteDrug(id){  
    this.drugService.deleteDrug(id)
      .subscribe(
        data => {
			if(data.res=='ok'){
				  this.toastrService.success('Drug deleted successfully');
				   this.getAllDrug();
			}
	          },
        error => {
          console.log(error);
        });
}
viewDrug(id){
  this.router.navigate(['add_drug', id]);
}

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { PasswordService } from '../common/services/password.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers: [PasswordService, ToastrService]
})
export class ResetPasswordComponent implements OnInit {
  password: string;
  otp: string;
  conpassword: string
  email: string;
  // error: string;
  constructor(private passwordService: PasswordService, private router: Router, route: ActivatedRoute, private toastrService: ToastrService, ) {

    this.otp = route.snapshot.params['otp'];
    console.log(this.otp);
    this.email = route.snapshot.params['email'];
    console.log(this.email);
  }

  ngOnInit() {}
  
  // reset user password 
  reset() {
    this.passwordService.updatePassword(this.password, this.otp, this.email).subscribe(
      data => {
        if (data.status == "fail") {
          this.toastrService.error("Error");
        } else {
          this.toastrService.success('Password updated Succesfully');
        }
      },
      error => {
        console.log(error);
      });

  }
}

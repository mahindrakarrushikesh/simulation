import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { OtpService } from './otp.service';


@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css'],
  providers: [ OtpService ]
})
export class OtpComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
   private varifyUrl = environment.apiUrl + 'api/varifyRegistrationCode';
   registration: string;
   userdetails: any;

  constructor(private otpService: OtpService, private toastrService: ToastrService, private router: Router) { }

  ngOnInit() { }
// validate code enter by user is valid or not 
  varify(){
    this.otpService.varify(this.registration)
    .subscribe(
      data => {
       if(data.isCorrect){
        this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
        this.userdetails.status = 'active';
        localStorage.setItem('userdetails', JSON.stringify(this.userdetails));
       this.toastrService.success(data.result);
        this.router.navigateByUrl('profile');
       }else{
        this.toastrService.error(data.result);
       }
   
      },
      error => {
        this.errorHandler(error);
      });
  }
// handle error which occure from api
  errorHandler(error){
		if (error.error && error.error.message == 'Token has expired') {
        // token expired then clear local storage
			this.toastrService.error(error.error.message);
			localStorage.clear();
			this.router.navigateByUrl('login');
		}
	}

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class OtpService {
  private varifyRegistrationCodeUrl = environment.apiUrl + 'api/varifyRegistrationCode';
  private simulation_user: any;
  private headers: any;
  private userdetails: any;

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
      if(this.userdetails.status != 'pending'){
        this.router.navigateByUrl('profile');
      }
      this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
      this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
    }
  }

  varify(code: string) {
    return this.http.post<any>(this.varifyRegistrationCodeUrl, { code: code }, { headers: this.headers })
      .map(data => {
        return data;
      });
  }

}

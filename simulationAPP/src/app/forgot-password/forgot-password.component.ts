import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { PasswordService } from '../common/services/password.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [PasswordService,ToastrService]
})
export class ForgotPasswordComponent implements OnInit {
  email: string;
  error: string;
  constructor(private passwordService: PasswordService,private toastrService: ToastrService,) { }

  ngOnInit() {
    this.error = '';
  }
  //varify email in present in databas or not and send password reset link 
  varify() {
    if (this.validate(this.email)) {
      this.error = '';
      this.passwordService.resetPassword(this.email).subscribe(
        data => {
		  if(data.status=="fail"){
			  console.log("No such record");
			    this.toastrService.error("No such User exist");
      }else{ 
			   this.toastrService.success("Email sent succesfully");
		  }
        },
        error => {
          console.log(error);
        });

    } else {
      this.error = 'invalid email';
    }

  }
// validate email id correct or not 
  validate(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return (true)
    }
    return (false)
  }

}

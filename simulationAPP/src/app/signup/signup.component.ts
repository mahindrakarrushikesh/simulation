import { Component, OnInit , ViewChild} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SignupService } from './signup.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
   providers: [SignupService, ToastrService]
})
export class SignupComponent implements OnInit {
  signupdata :any;
  reEnterPassword:  string;
  constructor(private SignupService: SignupService , private toastrService: ToastrService, private router: Router) { 
	if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {	
	}else{
		this.router.navigateByUrl('profile');
	}
  }
  ngOnInit() {
     this.signupdata = {} as any;
     this.reEnterPassword= '';
  } 
  // submit user sign in details 
	submitForm(){
	  this.SignupService.saveSignup(this.signupdata)
      .subscribe(
      data => {
        if (!data.token) {
          this.toastrService.error(data.result);
        } else {
          // if signup success then redirect user to varify page
          this.toastrService.success("signup success");
          this.router.navigateByUrl('varify');
        }
	  },
      error => {
		this.toastrService.error(error);
      });
	 }
}
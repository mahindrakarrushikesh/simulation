import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
@Injectable()
export class SignupService {

  private registerURL = environment.apiUrl + 'api/register';
  constructor(private http: HttpClient) { }

  saveSignup(signupdata: any) {
    return this.http.post<any>(this.registerURL, { user: signupdata })
      .map(data => {
        console.log(data);
        // login successful if there's a jwt token in the response
        if (data && data.token) {
          localStorage.setItem('simulation_user', JSON.stringify(data.token));
          localStorage.setItem('userdetails', JSON.stringify(data.result));
        }
        return data;
      });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class PopulationService {
	
   private getPopulationUrl = environment.apiUrl + 'api/population';
    private delPopulationURL = environment.apiUrl + 'api/del_population';
 
   private simulation_user:any;
   private headers:any;
   private userdetails: any;

  constructor(private http: HttpClient, private router: Router) { 
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		}else {
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
			this.simulation_user = 'Bearer '+JSON.parse(localStorage.getItem('simulation_user'));
			this.headers = new HttpHeaders({'Authorization': this.simulation_user});				
    }
    
    this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
    if(this.userdetails.role == 'admin'){
      this.router.navigateByUrl('profile');
    }
    if(this.userdetails.status == 'pending'){
      this.router.navigateByUrl('varify');
    }

	}

  getPopulation() {
    return this.http.get<any>(this.getPopulationUrl,{ headers: this.headers })
      .map(user => {
        return user;
      });
  }
   deletePopulation(id: number) {
	  return this.http.post<any>(this.delPopulationURL, {id : id},{ headers: this.headers })
        .map(user => {
            return user;
        });
	 }

}

import { Component, OnInit } from '@angular/core';
import { PopulationService } from './population.service';
import { Router } from '@angular/router';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-population',
  templateUrl: './population.component.html',
  styleUrls: ['./population.component.css'],
  providers: [PopulationService]
})
export class PopulationComponent implements OnInit {
  allPopulation: any = [];
  simulation_user:any;
  
  constructor( private population: PopulationService, private router: Router, private toastrService: ToastrService,) {
	if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
		this.router.navigateByUrl('login');
	}else {
		this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
	} 
 }

  ngOnInit() {
    // get all population data 
		this.getAllPopulation();
  }

// get all population from database 
  getAllPopulation() {
    this.population.getPopulation()
      .subscribe(
        data => {
           this.allPopulation = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
  // delete population using id 
  deletePopulaton(id) {
		this.population.deletePopulation(id)
			.subscribe(
				data => {
					if(data.affectedRows==1){
						this.toastrService.success('Population deleted successfully');
						this.getAllPopulation();
					}
					   
				},
				error => {
          this.errorHandler(error);
				});
				
  }
  viewPopulaton(id){
    this.router.navigate(['add_population', id])
  }
 // handle error which occure from api
  errorHandler(error){
		if (error.error && error.error.message == 'Token has expired') {
        // token expired then clear local storage
			this.toastrService.error(error.error.message);
			localStorage.clear();
			this.router.navigateByUrl('login');
		}
	}


}

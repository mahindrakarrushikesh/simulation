import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $ :any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {
	userdetails: any;
	private previousClass:any = '';
	constructor(private router: Router) {
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		  } else {
			this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
			console.log(this.userdetails);
		 }
	 }
	ngOnInit() {
		//[routerLink]="['/simulation_input', 1]";  
	 }
	 
	 fun1(){
		
		 //[routerLink]="['/user', user.id]"
	 }
	 
	 showHide(className){
		if(this.previousClass != '' && this.previousClass != className){
			 $('.'+this.previousClass).hide(500);
		 }
		 $('.'+className).toggle(500);
		 this.previousClass = className;
	 }
}

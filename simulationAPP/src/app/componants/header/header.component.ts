import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
	
	private simulation_user:any;
	public userdetails:any= [];
	 //public userdetails:any;
	 public username:any= [];
	 public firstname:any;
	constructor(private router: Router) { 
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		}else {
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
			 this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
			this.username=this.userdetails.firstname;
			
		}
	}
   logout(){
	   localStorage.clear();
	   this.router.navigateByUrl('login');
   }
}

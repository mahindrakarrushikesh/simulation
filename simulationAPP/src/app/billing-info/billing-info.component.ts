import { Component, OnInit } from '@angular/core';
import { Headers, RequestOptions} from "@angular/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-billing-info',
  templateUrl: './billing-info.component.html',
  styleUrls: ['./billing-info.component.css']
})
export class BillingInfoComponent implements OnInit {

	private simulation_user:any;
	constructor(private router: Router) {
		if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
			this.router.navigateByUrl('login');
		}else {
			this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
		}
	}

  ngOnInit() {
  }
}

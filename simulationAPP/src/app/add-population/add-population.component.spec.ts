import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPopulationComponent } from './add-population.component';

describe('AddPopulationComponent', () => {
  let component: AddPopulationComponent;
  let fixture: ComponentFixture<AddPopulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPopulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPopulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AddPopulationService {
  private addPopulationURL = environment.apiUrl + 'api/add_population';
  private getPopulationURL = environment.apiUrl + 'api/get_population';
  private updatePopulationURL = environment.apiUrl + 'api/update_population';
  private simulation_user: any;
  private headers: any;
  private userdetails: any;
// dependacy injection 
  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
      this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
      this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
    }
    this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
    if (this.userdetails.role == 'admin') {
      this.router.navigateByUrl('profile');
    }
    if (this.userdetails.status == 'pending') {
      this.router.navigateByUrl('varify');
    }

  }

// save population 
  savePolulation(populationData: any) {
    return this.http.post<any>(this.addPopulationURL, { populationData: populationData }, { headers: this.headers })
      .map(user => {
        return user;
      });
  }
  // get population from database using given id
  getPopulation(id: number) {
    return this.http.post<any>(this.getPopulationURL, { id: id }, { headers: this.headers })
      .map(user => {
        return user;
      });
  }
   // update population inside database using given id
  updatePolulation(populationData: any) {
    return this.http.post<any>(this.updatePopulationURL, { populationData: populationData }, { headers: this.headers })
      .map(user => {
        return user;
      });
  }



}

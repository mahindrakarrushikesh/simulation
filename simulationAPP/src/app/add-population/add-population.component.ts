import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AddPopulationService } from './add-population.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { ToolTipService } from '../common/services/tool-tip.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-population',
  templateUrl: './add-population.component.html',
  styleUrls: ['./add-population.component.css'],
  providers: [AddPopulationService, ToastrService, ToolTipService]

})
export class AddPopulationComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  tools: any;
  tooltippopulation: any;
  id: number;
  // classes of population
  blood_flow: any;
  tissue_volumn: any;
  population: any;
  populationName: any;
  populationData: any;
  simulation_user: any;
  public loading = false;
// dependacy injection 
  constructor(private populationService: AddPopulationService, private toastrService: ToastrService, private router: Router, route: ActivatedRoute, private tooltip: ToolTipService) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
      this.id = route.snapshot.params['id'];
      if (this.id) {
        // get population using id for edit or view 
        this.getPopulation();
      }

    }
  }

 

  ngOnInit() {
    //init population data 
    this.blood_flow = {} as any;
    this.tissue_volumn = {} as any;
    this.population = {} as any;

    // get tool tip data from json file

    this.tooltippopulation = {} as any;
    this.tooltippopulation.blood_flow = {} as any;
    this.tooltippopulation.tissue_volumn = {} as any;
    // get tootl tip data 
     this.gettootip();
  }


  // get population using id for edit or view
  getPopulation() {
    this.populationService.getPopulation(this.id)
      .subscribe(
        data => {
          this.blood_flow = JSON.parse(data[0]['blood_flow']);
          this.tissue_volumn = JSON.parse(data[0]['tissue_volumn']);
          this.populationName = data[0]['populationName'];
        },
        error => {
          this.errorHandler(error);
        });
  }
  // get tootl tip data 
  public gettootip() {
    this.tooltip.getToolTopData()
      .subscribe(
        data => {
          this.tooltippopulation = data.population.subject;
        },
        error => {
          this.errorHandler(error);
        });
  }
  // save population to database 
  submitForm() {
    // preprocess population data before save 
    this.loading = true;
    if (this.id != undefined) {//If existing population is updated

      this.populationData = { 'id': this.id, 'populationName': this.populationName, 'blood_flow': this.blood_flow, 'tissue_volumn': this.tissue_volumn };
     // update population if exsting 
      this.populationService.updatePolulation(this.populationData)
        .subscribe(
          data => {
      this.loading = false;
            if (data.result == 'Population updated successfully') {
              this.toastrService.success('Population updated successfully');
            } else {
              this.toastrService.error(data);
            }
          },
          error => {
            this.loading = false;
            this.errorHandler(error);
          });
    } else {
      //If new population is added
      this.populationData = { 'populationName': this.populationName, 'blood_flow': this.blood_flow, 'tissue_volumn': this.tissue_volumn };
      this.populationService.savePolulation(this.populationData)
        .subscribe(
          data => {
            this.loading = false;
            this.toastrService.success('population addedd successfully');
          },
          error => {
            this.errorHandler(error);
          });

    }
  }
  // handle error which occure from api
  errorHandler(error) {
    if (error.error && error.error.message == 'Token has expired') {
      // token expired then clear local storage 
      this.toastrService.error(error.error.message);
      localStorage.clear();
      this.router.navigateByUrl('login');
    }
  }

}

import { TestBed, inject } from '@angular/core/testing';

import { AddPopulationService } from './add-population.service';

describe('AddPopulationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddPopulationService]
    });
  });

  it('should be created', inject([AddPopulationService], (service: AddPopulationService) => {
    expect(service).toBeTruthy();
  }));
});

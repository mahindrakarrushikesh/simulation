import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class PasswordService {

  private resetPasswordRequestUrl = environment.apiUrl + 'api/resetPasswordRequest';
 //private updatePasswordUrl = environment.apiUrl + 'api/resetPasswordRequest';
 private updatePasswordUrl = environment.apiUrl + 'api/updatePasswordRequest';
 constructor(private http: HttpClient) {

   }


   resetPassword(email:string) {
    return this.http.post<any>(this.resetPasswordRequestUrl, { email: email})
      .map(data => {
        return data;
      });
} 

  updatePassword(password:string, otp:string,email:string) {
    return this.http.post<any>(this.updatePasswordUrl, { password: password,otp:otp,email:email})
      .map(data => { 
        return data;
      });
  }

}

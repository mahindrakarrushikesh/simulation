import { TestBed, inject } from '@angular/core/testing';

import { PasswordServiceService } from './password-service.service';

describe('PasswordServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PasswordServiceService]
    });
  });

  it('should be created', inject([PasswordServiceService], (service: PasswordServiceService) => {
    expect(service).toBeTruthy();
  }));
});

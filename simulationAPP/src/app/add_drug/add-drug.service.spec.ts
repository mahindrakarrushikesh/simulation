import { TestBed, inject } from '@angular/core/testing';

import { AddDrugService } from './add-drug.service';

describe('AddDrugService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddDrugService]
    });
  });

  it('should be created', inject([AddDrugService], (service: AddDrugService) => {
    expect(service).toBeTruthy();
  }));
}); 

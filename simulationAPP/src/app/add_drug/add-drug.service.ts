import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AddDrugService {

    // services
    private addNewDrug = environment.apiUrl + 'api/addDrug';
    private updateDrugInfo = environment.apiUrl + 'api/updateDrug';
    private getDrugById = environment.apiUrl + 'api/getDrugById';
    private simulation_user: any;
    private headers: any;
    private userdetails: any;
    // inject dependency in services 
    constructor(private http: HttpClient, private router: Router) {
        if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
            this.router.navigateByUrl('login');
        } else {
            // get token from local storage and set in header 
            this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
            this.simulation_user = 'Bearer ' + JSON.parse(localStorage.getItem('simulation_user'));
            this.headers = new HttpHeaders({ 'Authorization': this.simulation_user });
        }
        this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
        if(this.userdetails.role == 'admin'){
            this.router.navigateByUrl('profile');
          }
        if(this.userdetails.status == 'pending'){
            this.router.navigateByUrl('varify');
          }
    }
// call api to send data to save in database 
    saveDrug(drug: any, type: string) {
        return this.http.post<any>(this.addNewDrug, { drug: drug, type: type }, { headers: this.headers })
            .map(data => {
                return data;
            });
    }
    // call api to send data to update in database 
    updateDrug(id: number, drug: any, type: string) {
        return this.http.post<any>(this.updateDrugInfo, { id: id, drug: drug, type: type }, { headers: this.headers })
            .map(data => {
                return data;
            });
    }
        // call api to get drug of given from database
    getDrug(id: number) {
        return this.http.post<any>(this.getDrugById, { id: id }, { headers: this.headers })
            .map(data => {
                return data;
            });
    }

} 
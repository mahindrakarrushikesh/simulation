import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AddDrugService } from './add-drug.service';
import { ToolTipService } from '../common/services/tool-tip.service';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add_drug',
  templateUrl: './add_drug.component.html',
  styleUrls: ['./add_drug.component.css'],
  providers: [AddDrugService, ToastrService, ToolTipService]
})

export class AddDrugComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  // attributes of UI
  drug: any;
  select_absorption: string = 'dlm';
  select_distribution: string = 'mini_pbpk';
  jsondata: any;
  tooltippopulationdrug: any;
  alldissolution: any;
  addDrugForm: FormGroup;
  first_order: any;
  simulation_user: any;
  id: number;
  tooltippopulation: any;
  dlmParticleError: string;
  dlmParticleTestValid: boolean;
  study_type: string;
  public loading = false;
  testValid: boolean = false;
  dlmDiTestValid: boolean;
  dlmDissolutionError: string;
  validTest: boolean;
  testCaseError: string;
  constructor(private http: Http, private fb: FormBuilder, private drugService: AddDrugService, private toastrService: ToastrService, private router: Router, route: ActivatedRoute, private tooltip: ToolTipService) {
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {
      this.router.navigateByUrl('login');
    } else {
      this.simulation_user = JSON.parse(localStorage.getItem('simulation_user'));
      this.id = route.snapshot.params['id'];
      if (this.id) {
        this.getDrug();
      }
    }
  }

  ngOnInit() {

    //init objects here
    this.drug = {} as any;
    this.drug.physchem = {} as any;
    this.drug.absorption = {} as any;
    this.drug.absorption.dlm = {} as any;
    this.drug.absorption.dlm.dissolution = [];
    this.drug.absorption.dlm.dissolution.push({});
    this.drug.absorption.dlm.particle = [];
    this.drug.absorption.dlm.particle.push({});

    this.drug.absorption.adam = {} as any;
    this.drug.absorption.adam.dissolution = [];
    this.drug.absorption.adam.dissolution.push({});
    this.drug.absorption.first_order = {} as any;

    this.drug.metabolism = {} as any;
    this.drug.metabolism.mini_pbpk = {} as any;
    this.drug.metabolism.full_pbpk = {} as any;
    this.drug.metabolism.vbe = {} as any;



    this.drug.distribution = {} as any;
    this.drug.distribution.mini_pbpk = {} as any;
    this.drug.distribution.full_pbpk = {} as any;
    this.drug.distribution.vbe = {} as any;


    // init objects for tooltips 
    this.tooltippopulation = {} as any;
    this.tooltippopulation.drug = {} as any;
    this.tooltippopulation.drug.physchem = {} as any;
    this.tooltippopulation.drug.absorption = {} as any;
    this.tooltippopulation.drug.absorption.dlm = {} as any;

    this.tooltippopulation.drug.absorption.adam = {} as any;
    this.tooltippopulation.drug.absorption.first_order = {} as any;

    this.tooltippopulation.drug.distribution = {} as any;
    this.tooltippopulation.drug.distribution.full_pbpk = {} as any;
    this.tooltippopulation.drug.distribution.mini_pbpk = {} as any;
    this.tooltippopulation.drug.distribution.vbe = {} as any;

    this.tooltippopulation.drug.metabolism = {} as any;
    this.tooltippopulation.drug.metabolism.full_pbpk = {} as any;
    this.tooltippopulation.drug.metabolism.mini_pbpk = {} as any;
    this.tooltippopulation.drug.metabolism.vbe = {} as any;


    this.gettootip();

  }

  getDrug() {
    /* @desc: get drug details from DB
     @input: drug id
   @returns: drug details for edit or view */
    this.drugService.getDrug(this.id)
      .subscribe(
        data => {
          this.drug = JSON.parse(data.drugdetails);
          if (this.drug.distribution.vbe) {
            this.select_distribution = 'vbe';
          } else if (this.drug.distribution.mini_pbpk) {
            this.select_distribution = 'mini_pbpk';
          } else {
            this.select_distribution = 'full_pbpk';
          }
          if (this.drug.absorption.first_order) {
            this.select_absorption = 'first_order';
          } else if (this.drug.absorption.adam) {
            this.select_absorption = 'adam';
          } else {
            this.select_absorption = 'dlm';
          }

        },
        error => {
          this.errorHandler(error);
        });
  }
  // add dissolution in dissolution array 
  addDissolution() {
    let array = [];
    array = this.drug.absorption.adam.dissolution;
    let solution = {} as any;
    solution.time = null;
    solution.test = null;
    solution.reference = null;
    array.push(solution);
    this.drug.absorption.adam.dissolution = array;
  }
  // remove dissolution from array 
  removeDissolution(index: number) {
    this.drug.absorption.adam.dissolution.splice(index, 1);
  }
  // add dml dissolution in array 
  addDlmDissolution() {
    let array = [];
    array = this.drug.absorption.dlm.dissolution;
    let solution = {} as any;
    solution.release_time = null;
    solution.release_fraction = null;
    array.push(solution);
    this.drug.absorption.dlm.dissolution = array;
  }
  // remove dlm dissolution from array 
  removeDlmDissolution(index: number) {
    this.drug.absorption.dlm.dissolution.splice(index, 1);
  }
  // add Particle dissolution in array 
  addParticleDissolution() {
    let array = [];
    array = this.drug.absorption.dlm.particle;
    let solution = {} as any;
    solution.particle_radius = null;
    solution.particle_fraction = null;
    array.push(solution);
    this.drug.absorption.dlm.particle = array;
  }
  // remove Particle dissolution from array 
  removeParticleDissolution(index: number) {
    this.drug.absorption.dlm.particle.splice(index, 1);
  }
  // get tooltip data for drug object from tooltip.json file 
  public gettootip() {
    this.tooltip.getToolTopData()
      .subscribe(
        data => {
          this.tooltippopulation = data;
        },
        error => {
          this.errorHandler(error);
        });
  }
  // submit drug to save or update in database 
  submitForm() {
    // set loader active while save 
    this.loading = true;
    // preprocess data before save 
    if (this.select_absorption == 'adam') {
      delete this.drug.absorption.first_order;
      delete this.drug.absorption.dlm;
    } else if (this.select_absorption == 'dlm') {
      delete this.drug.absorption.first_order;
      delete this.drug.absorption.adam;
    } else if (this.select_absorption == 'first_order') {
      delete this.drug.absorption.dlm;
      delete this.drug.absorption.adam;
    }

    if (this.select_distribution == 'full_pbpk') {
      delete this.drug.metabolism.vbe;
      delete this.drug.metabolism.mini_pbpk;
      delete this.drug.distribution.vbe;
      delete this.drug.distribution.mini_pbpk;
      this.study_type = "pk_study";
    } else if (this.select_distribution == 'vbe') {
      delete this.drug.metabolism.full_pbpk;
      delete this.drug.metabolism.mini_pbpk;
      delete this.drug.distribution.full_pbpk;
      delete this.drug.distribution.mini_pbpk;
      this.study_type = "vbe_study";
    } else if (this.select_distribution == 'mini_pbpk') {
      delete this.drug.metabolism.full_pbpk;
      delete this.drug.metabolism.vbe;
      delete this.drug.distribution.full_pbpk;
      delete this.drug.distribution.vbe;
      this.study_type = "pk_study";
    }


    if (this.id) {
      // if existing drug then update 
      this.drugService.updateDrug(this.id, this.drug, this.study_type)
        .subscribe(
          data => {
            this.loading = false;
            this.toastrService.success('Drug updated successfully');
          },
          error => {
            this.loading = false;
            this.errorHandler(error);
          });

    } else {
      // save if new drug
      this.drugService.saveDrug(this.drug, this.study_type)
        .subscribe(data => {
          this.loading = false;
          this.toastrService.success('drug added successfully');
        }, error => {
          this.loading = false;
          this.errorHandler(error);
        });
    }
  }
  // called when absorption drop down selected 
  onAbsorptionSelect() {
    if (this.select_absorption == 'adam') {
      this.drug.absorption.adam = {} as any;
      this.drug.absorption.adam.dissolution = [];
      this.drug.absorption.adam.dissolution.push({});
      this.drug.absorption.first_order = {} as any;
      this.testValid = true;
      this.dlmParticleTestValid = false;
      this.dlmDiTestValid = false;
    } else if (this.select_absorption == 'dlm') {
      this.drug.absorption.dlm = {} as any;
      this.drug.absorption.dlm.dissolution = [];
      this.drug.absorption.dlm.dissolution.push({});
      this.drug.absorption.dlm.particle = [];
      this.drug.absorption.dlm.particle.push({});
      this.testValid = false;
      this.dlmParticleTestValid = true;
      this.dlmDiTestValid = true;
    } else {
      this.drug.absorption.first_order = {} as any;
      this.testValid = false;
      this.dlmParticleTestValid = false;
      this.dlmDiTestValid = false;
    }
  }

  // called when Distibution drop down selected 
  onDistibutionSelect() {
    if (this.select_distribution == 'mini_pbpk') {
      this.drug.metabolism.mini_pbpk = {} as any;
      this.drug.distribution.mini_pbpk = {} as any;
    } else if (this.select_distribution == 'full_pbpk') {
      this.drug.metabolism.full_pbpk = {} as any;
      this.drug.distribution.full_pbpk = {} as any;
    } else {
      this.drug.metabolism.vbe = {} as any;
      this.drug.distribution.vbe = {} as any;
    }
  }
  // validate dissolution test input
  validateTestCase() {
    this.testCaseError = "";
    this.testValid = true;
    for (let i = 0; i < this.drug.absorption.adam.dissolution.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.adam.dissolution[i - 1].time > this.drug.absorption.adam.dissolution[i].time) {
          this.testCaseError = "time value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i - 1].test > this.drug.absorption.adam.dissolution[i].test) {
          this.testCaseError = "test value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i - 1].reference > this.drug.absorption.adam.dissolution[i].reference) {
          this.testCaseError = "reference value should be grater than previous";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].test > 100) {
          this.testCaseError = "test should be less than 100";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].reference > 100) {
          this.testCaseError = "reference should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.adam.dissolution[i].test > 100) {
          this.testCaseError = "test should be less than 100";
          return false;
        } else if (this.drug.absorption.adam.dissolution[i].reference > 100) {
          this.testCaseError = "reference should be less than 100";
          return false;
        }
      }
    }

    this.testValid = false;

  }
  // handle error which occure from api
  errorHandler(error) {
    // token expired then clear local storage 
    if (error.error && error.error.message == 'Token has expired') {
      this.toastrService.error(error.error.message);
      localStorage.clear();
      this.router.navigateByUrl('login');
    }
  }
  // validate  Dissolution test cases for each input
  validateDissolution() {
    this.dlmDissolutionError = "";
    this.dlmDiTestValid = true;
    for (let i = 0; i < this.drug.absorption.dlm.dissolution.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.dlm.dissolution[i - 1].release_time > this.drug.absorption.dlm.dissolution[i].release_time) {
          this.dlmDissolutionError = "release time value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.dissolution[i - 1].release_fraction > this.drug.absorption.dlm.dissolution[i].release_fraction) {
          this.dlmDissolutionError = "release fraction value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.dissolution[i].release_fraction > 100) {
          this.dlmDissolutionError = "release fraction should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.dlm.dissolution[i].release_fraction > 100) {
          this.dlmDissolutionError = "release fraction should be less than 100";
          return false;
        }
      }
    }
    this.dlmDiTestValid = false;
  }


  // validate particle test cases for each input
  validateParticle() {
    this.dlmParticleError = "";
    this.dlmParticleTestValid = true;
    for (let i = 0; i < this.drug.absorption.dlm.particle.length; i++) {
      if (i > 0) {
        if (this.drug.absorption.dlm.particle[i - 1].particle_radius > this.drug.absorption.dlm.particle[i].particle_radius) {
          this.dlmParticleError = "Particle radius value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.particle[i - 1].particle_fraction > this.drug.absorption.dlm.particle[i].particle_fraction) {
          this.dlmParticleError = "Particle fraction value should be grater than previous";
          return false;
        } else if (this.drug.absorption.dlm.particle[i].particle_fraction > 100) {
          this.dlmParticleError = "Release fraction should be less than 100";
          return false;
        }
      } else {
        if (this.drug.absorption.dlm.particle[i].particle_fraction > 100) {
          this.dlmParticleError = "Particle fraction should be less than 100";
          return false;
        }
      }
    }
    this.dlmParticleTestValid = false;
  }

}

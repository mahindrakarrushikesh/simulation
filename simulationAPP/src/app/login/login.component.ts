import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService, ToastrService]
})
export class LoginComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  model: any = {};
  userdetails: any;

  constructor(private auth: AuthenticationService, private toastrService: ToastrService, private router: Router) {
    console.log(localStorage.getItem('simulation_user'));
    if (localStorage.getItem('simulation_user') === undefined || localStorage.getItem('simulation_user') == null) {

    } else {
      this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
      if (!this.userdetails.status) {
        this.router.navigateByUrl('varify');
      } else {
        this.router.navigateByUrl('profile');
      }
   }

  }
  ngOnInit() {

  }

  login() {
    //validate user is presend in system using company name, email and password 
    this.auth.login(this.model)
      .subscribe(
        data => {
          if (!data.token) {
            // show error message 
            this.toastrService.error(data.result);
          } else {
            // if details are valid redirect user to profile page 
            this.toastrService.success("logged in successfully");
            this.router.navigateByUrl('profile');
          }
        },
        error => {
          console.log(error);
        });
  }

}

module.exports = function (req, res, next) {
    var allowedOrigins = ['http://localhost:4200','http://192.168.100.13:4200'];
  var origin = req.headers.origin; 
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
}
  
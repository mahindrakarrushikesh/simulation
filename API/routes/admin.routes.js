module.exports = function(app) {

var checkAuthenticated = require('../middleware/checkAuthenticated');

var user = require('../controllers/admin/user');

app.get('/api/admin/getUsers',  checkAuthenticated, user.getUsers);
app.post('/api/admin/getUser',  checkAuthenticated, user.getUserById);
app.post('/api/admin/deleteUser', checkAuthenticated, user.deleteUser);
app.post('/api/admin/addUser', checkAuthenticated, user.addUser);
app.post('/api/admin/updateUser', checkAuthenticated, user.updateUser);
app.post('/api/admin/deleteUser', checkAuthenticated, user.deleteUser);

}
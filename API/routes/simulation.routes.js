module.exports = function(app) {

var checkAuthenticated = require('../middleware/checkAuthenticated');

var user = require('../controllers/user');
var popuation = require('../controllers/popuation'); 
var trial = require('../controllers/trial'); 
var simulation = require('../controllers/simulation'); 
var profile = require('../controllers/profile'); 

app.post('/api/register', user.register);
app.post('/api/login', user.login);
app.post('/api/varifyRegistrationCode', checkAuthenticated, user.varifyRegistrationCode);
app.post('/api/add_population', checkAuthenticated,popuation.add_population);
app.post('/api/get_population', checkAuthenticated,popuation.get_population);
app.post('/api/del_population', checkAuthenticated,popuation.del_population);
app.post('/api/update_population', checkAuthenticated,popuation.update_population);
app.post('/api/add_trial', checkAuthenticated, trial.add_trial);
app.post('/api/get_trial', checkAuthenticated, trial.get_trial);
app.post('/api/update_trial', checkAuthenticated, trial.update_trial);
app.post('/api/deletetrialDesign', checkAuthenticated, trial.deletetrialDesign);
app.post('/api/addDrug', checkAuthenticated, simulation.addDrug); 
app.post('/api/updateDrug', checkAuthenticated, simulation.updateDrug); 
app.post('/api/getDrugById',checkAuthenticated, simulation.getexDrug); 
app.post('/api/deletedrug', checkAuthenticated,simulation.deletedrug);
app.post('/api/addUser', checkAuthenticated, profile.addUser);
app.post('/api/setPasswordInfo', checkAuthenticated, profile.setPasswordInfo);
app.post('/api/addUseraddress', checkAuthenticated, profile.setUseraddress);
app.get('/api/population', checkAuthenticated, simulation.getAllPopulation);
app.post('/api/trialDesign', checkAuthenticated, simulation.getAllTrialDesignWithType);
app.get('/api/trialDesign', checkAuthenticated, simulation.getAllTrialDesign);
app.get('/api/myInfo', checkAuthenticated,profile.getProfileInfo); 
app.post('/api/addNewSimulation', checkAuthenticated, simulation.addNewSimulation);
app.post('/api/getsimdataUrl', checkAuthenticated, simulation.getsimdata);
app.post('/api/drug', checkAuthenticated, simulation.getAllDrugWithType);
app.get('/api/drug', checkAuthenticated, simulation.getAllDrug);
app.get('/api/simulation', checkAuthenticated, simulation.getAllSimulation);
app.post('/api/getGraphData', checkAuthenticated, simulation.getSimulation);
app.post('/api/resetPasswordRequest', profile.resetPasswordRequest);
app.post('/api/updatePasswordRequest', profile.updatePasswordRequest);
app.post('/api/generateReportPdf', simulation.generateReportPdf);
app.post('/api/generateCSV', simulation.generateCSV);


}
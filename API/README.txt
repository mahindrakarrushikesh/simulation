# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version
express: 4.16.2
mysql: 2.15.0

### How do I get set up? ###

* Summary of set up
Install all npm packages listed in package.json
command to install all packages => npm install


* Configuration
all configuration are in config/simulation.config.json
you can change http port => PORT,
you can change database path =>	PASSWORD
you can change database name => DB
you can change database user =>	USER
you can change token validity time => token_validity


* Database configuration
database configuration values are in config/simulation.config.json file 
user can change database name, username , password from config file as per your server 

* How to run api
command =>  node server.js





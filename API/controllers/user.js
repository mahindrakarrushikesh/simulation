var jwt = require('jwt-simple');
var assert = require('assert');
var mysql = require('mysql');
var moment = require('moment');
var secret = "simulation_test_key"
//config
var config = require('../config/simulation.config');


var con = mysql.createConnection({
    host: config.HOST
    , user: config.USER
    , password: config.PASSWORD
    , database: config.DB
}); 

con.connect(function (err) {
    if (err) throw err;
    console.log("Connected @ user.js");
});

module.exports = {
    register: function (req, res) {//register
		if(!req.body.user){
	     res.status(500).send({ error: 'something went wrong' });  
     }
		con.connect(function(err) {
		 var sql = "INSERT INTO user (firstname, lastname, email, company, username, password, status) VALUES ('"+ req.body.user.firstname +"','"+ req.body.user.lastname +"','"+ req.body.user.email +"','"+ req.body.user.companyname +"','"+ req.body.user.username +"','"+ req.body.user.password +"','pending')";
	con.query(sql, function (err, result) { 
	 var obj = {};
   if (err){
	   if(err.code == 'ER_DUP_ENTRY'){
         obj.result = "Email or username already exists"; 
	     res.send(obj);
	   }else{
	     obj.result = "Error unable to register";
	     res.send(obj);
		}
      }else{
		 req.body.user.status = "pending";
		 req.body.user.role = "user";
		obj.result = req.body.user;
		obj.token = generateToken(req.body.user);
		res.send(obj);  
	 }
   });
});
},
login: function (req, res) { //login
   var user = req.body.user;
    con.connect(function(err) {
    var sql = "SELECT * from user where username= '"+ user.username +"' AND password= '"+ user.password +"' AND company= '"+ user.company +"'";
	con.query(sql, function (err, result) { 
   if (err) res.send(err);
   var obj = {};
   if(result.length > 0){
	   if(result[0].status == 'inactive'){
		 obj.result = "your access is restricted by admin. Please conact admin."; 
	     res.send(obj);
	   }else{
		obj.result = result[0];
		obj.token = generateToken(result[0]);
		res.send(obj);
	   }
   }else{
	   obj.result = "inavlid user details"; 
	   res.send(obj);
   }
  });
  
});

},
   varifyRegistrationCode: function(req, res){//varify
	   
		con.connect(function(err) {
			
	var sql = "SELECT * from user where username = '"+ req.body.auth +"'";
	con.query(sql, function (err, result) { 
    if (err) res.send(err);
    var obj = {};
	if(result.length == 1){
		if(req.body.code == 'valid003'){
	   correctVarificationCode(req, res);	
		}else{
	   obj.result = "Wrong varification code"; 
	   res.send(obj);	
		}
	}else{
	   obj.result = "Something went wrong"; 
	   res.send(obj);	
	}
	
  });
});
}
}


function generateToken(details){
    var payload = {
        sub: details.username
        , iat: moment().unix()
        , exp: moment().add(config.token_validity, 'm').unix()
    };
    return jwt.encode(payload, config.key);
}

function correctVarificationCode(req, res){
	 con.connect(function(err) { 
		 var sql="UPDATE user SET status = 'active' WHERE username='"+req.body.auth+"'";
		 con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		   var obj = {};
		    obj.isCorrect = true;
		    obj.result = "varification successfull"; 
	        res.send(obj);  
		   });
         });
    }



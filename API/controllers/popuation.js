var jwt = require('jwt-simple');
var assert = require('assert');
var mysql = require('mysql');

//config
var config = require('../config/simulation.config');


var con = mysql.createConnection({
    host: config.HOST
    , user: config.USER
    , password: config.PASSWORD
    , database: config.DB
}); 

con.connect(function (err) {
    if (err) throw err;
});

module.exports = {
    add_population: function (req, res) { //add population
		var obj = {};
		if(!req.body.populationData){
			obj.result = "Please provide some data";
	        res.send(obj);
		}
		var blood_flow = JSON.stringify(req.body.populationData.blood_flow);
		console.log(blood_flow);
		var tissue_volumn = JSON.stringify(req.body.populationData.tissue_volumn);
		con.connect(function(err) {
		 var sql = "INSERT INTO population (user, populationName, blood_flow, tissue_volumn) VALUES ('"+req.body.auth+"','"+req.body.populationData.populationName+"','"+blood_flow+"','"+tissue_volumn+"')";
			con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		   
		   obj.result = "Population added successfully";
	        res.send(obj);
		   });
         });
 }, update_population: function (req, res) {//update population
	 var obj = {};
		if(!req.body.populationData){
			obj.result = "Please provide some data";
	        res.send(obj);
		}
		
		var blood_flow = JSON.stringify(req.body.populationData.blood_flow);
		console.log(blood_flow);
		var tissue_volumn = JSON.stringify(req.body.populationData.tissue_volumn);
		con.connect(function(err) {
		 var sql = "UPDATE population SET  populationName='"+req.body.populationData.populationName+"', blood_flow='"+blood_flow+"', tissue_volumn='"+tissue_volumn+"', created_on='"+formatDate()+"' where user='"+req.body.auth+"' and id='"+req.body.populationData.id+"'";
			con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		   
		   obj.result = "Population updated successfully";
	        res.send(obj);
		   });
         });
 },
del_population: function (req, res) { //delete population
	var obj = {};
		if(!req.body.id){
			obj.result = "Please provide id";
	        res.send(obj);
		}
	  var sql = "delete from population where user='"+req.body.auth+"'&&id='"+req.body.id+"'";
		con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		   res.send(result);
		   });
        
 },
 get_population: function (req, res) { //get population
	  var sql = "select *  from population where user='"+req.body.auth+"'&&id='"+req.body.id+"'";
		con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		   res.send(result);
		   });
        
 } 
    
}

function formatDate(val){
	if(val){
		var date = new Date(val);
	}else{
		var date = new Date();
	}
	
	var dd = date.getDate();
var mm = date.getMonth()+1; 
var yyyy = date.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
} 
return yyyy+'-'+mm+'-'+dd;
}
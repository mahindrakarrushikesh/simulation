var mysql = require('mysql');

//config
var config = require('../config/simulation.config');


var con = mysql.createConnection({
    host: config.HOST
    , user: config.USER
    , password: config.PASSWORD
    , database: config.DB
}); 

con.connect(function (err) {
    if (err) throw err;
});

module.exports = {
    add_trial: function (req, res) { // add trial
		if(!req.body.trial){
	     res.status(500).send({ error: 'something went wrong' })  
     }
		var trialdata = JSON.stringify(req.body.trial);
		con.connect(function(err) {
		 var sql="INSERT INTO trial (user, trialName, trialData, type) VALUES ('"+req.body.auth+"','"+req.body.trialName+"','"+trialdata+"','"+req.body.trialType+"')";
	
			con.query(sql, function (err, result) { 
		      if (err) {
			   console.log(err);
			   res.send(err)
		      }		
				res.send({'res': 'ok'});  
		   });
         });
     },
	 update_trial: function (req, res) {// update trial
		if(!req.body.trial){
	     res.status(500).send({ error: 'something went wrong' })  
     }
		var trialdata = JSON.stringify(req.body.trial);
		con.connect(function(err) {
		 var sql="UPDATE trial SET trialName='"+req.body.trialName+"',trialData='"+trialdata+"',type='"+req.body.trialType+"', created_on='"+formatDate()+"'  where user='"+req.body.auth+"'and id='"+req.body.id+"'";
		 console.log(sql);
			con.query(sql, function (err, result) { 
		      if (err) {
			   console.log(err);
			   res.send(err)
		      }		
				res.send({'res': 'ok'});  
		   });
         });
     }, 
      deletetrialDesign: function (req, res) {// delete trial
		con.connect(function(err) {
		 var sql="Delete from trial where id='"+req.body.id+"'";
			con.query(sql, function (err, result) { 
		   if (err) res.send(err);
		    	   res.send({
					'res': 'ok'
				});
		   });
         });
	 }
    , get_trial: function (req, res) {// get trial
		con.connect(function (err) {
            var sql = "SELECT * FROM `trial` where id = '"+ req.body.id +"'";
			console.log(sql);
            con.query(sql, function (err, result) {
                if (err) res.send(err);
                res.send(result[0]);
            });
        });
    }
	
}


function formatDate(val){
	if(val){
		var date = new Date(val);
	}else{
		var date = new Date();
	}
	var dd = date.getDate();
    var mm = date.getMonth()+1; 
    var yyyy = date.getFullYear();
    if(dd<10){
       dd='0'+dd;
    } 
    if(mm<10){
       mm='0'+mm;
    } 
    return yyyy+'-'+mm+'-'+dd;
}

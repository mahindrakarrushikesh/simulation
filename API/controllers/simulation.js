var mysql = require('mysql');
var request = require('request');
//config
var fs = require('fs');
var pdf = require('html-pdf');

var config = require('../config/simulation.config');


var con = mysql.createConnection({
    host: config.HOST,
    user: config.USER,
    password: config.PASSWORD,
    database: config.DB
});
con.connect(function(err) {
    if (err) throw err;
});

module.exports = {
    getAllPopulation: function(req, res) { //get all Population info
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM `population` where user = '" + req.body.auth + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getsimdata: function(req, res) {
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM simulations WHERE  id ='" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result[0]);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    addDrug: function(req, res) { // add drug
        try {
            if (!req.body.drug) {
                res.status(400).send({
                    'res': 'Please send data'
                });
            }
            var drugdetails = JSON.stringify(req.body.drug);
            con.connect(function(err) {
                var sql = "INSERT INTO drug (user,name,drugdetails, type) VALUES ('" + req.body.auth + "','" + req.body.drug.name + "','" + drugdetails + "','" + req.body.type + "')";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send({
                        'res': 'ok'
                    });
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    updateDrug: function(req, res) {
        try {
            if (!req.body.drug) {
                res.status(400).send({
                    'res': 'Please send data'
                });
            }
            var drugdetails = JSON.stringify(req.body.drug);
            con.connect(function(err) {
                var sql = "UPDATE drug SET name = '" + req.body.drug.name + "', drugdetails='" + drugdetails + "', type='" + req.body.type + "', created_on='" + formatDate() + "'  where user = '" + req.body.auth + "' and id = '" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send({
                        'res': 'ok'
                    });
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    deletedrug: function(req, res) { //delete drug
        try {
            con.connect(function(err) {
                var sql = "Delete from drug where user ='" + req.body.auth + "'&&id='" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send({
                        'res': 'ok'
                    });
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getexDrug: function(req, res) {
        try {
            var obj = {}
            if (!req.body.id) {
                obj.error = "Please provide id";
                res.send(obj);
            }
            con.connect(function(err) {
                var sql = "SELECT * FROM `drug` where user = '" + req.body.auth + "'&& id = '" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result[0]);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getAllTrialDesign: function(req, res) { //get all trial design
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM trial where user = '" + req.body.auth + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getAllTrialDesignWithType: function(req, res) { //get all trial with type
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM trial where user = '" + req.body.auth + "' AND type = '" + req.body.type + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getSimulation: function(req, res) {
        try {
            if (!req.body.id) {
                res.status(400).send("Please provide id");
            }
            con.connect(function(err) {
                var sql = "SELECT * FROM `simulationdata` where id='" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getAllDrug: function(req, res) { //get All Drug info
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM `drug` where user = '" + req.body.auth + "'";

                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getAllDrugWithType: function(req, res) { //get all drugs with type
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM `drug` where user = '" + req.body.auth + "' AND type = '" + req.body.type + "'";

                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    addNewSimulation: function(req, res) { //add new simulation
        try {
            if (!req.body.data) {
                res.status(400).send("Please provide input");
            }


            req.body.time.trial_timestamp = formatDate(req.body.time.trial_timestamp);
            req.body.time.population_timestamp = formatDate(req.body.time.population_timestamp);
            req.body.time.drug_timestamp = formatDate(req.body.time.drug_timestamp);

            var data = JSON.stringify(req.body.data);
            con.connect(function(err) {
                var simulation = JSON.stringify(req.body.data);
                var sql = "INSERT INTO simulations (user, type, simulation_data, trial_name, population_timestamp, drug_timestamp, trial_timestamp) VALUES ('" + req.body.auth + "','" + req.body.studyType + "','" + simulation + "','" + req.body.trial_name + "','" + req.body.time.population_timestamp + "','" + req.body.time.drug_timestamp + "','" + req.body.time.trial_timestamp + "')";

                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    req.body.insertId = result.insertId;

                    request.post({
                        headers: {
                            'content-type': 'application/json',
                            accept: 'application/json'
                        },
                        url: 'http://api.b2osim.com/api/v1/12345678/simulation',
                        body: data,
                        method: 'POST',
                    }, function(error, response, body) {
                        if (error) {
                            res.status(500).send('invalid input')
                        } else {
                            saveSimulationData(req, res, body)
                        }

                    });

                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    getAllSimulation: function(req, res) {
        try {
            con.connect(function(err) {
                var sql = "SELECT simulations.*, simulationdata.id, simulationdata.inputid, simulationdata.type FROM simulationdata INNER JOIN simulations ON simulationdata.inputid=simulations.id where simulationdata.user = '" + req.body.auth + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    res.send(result);
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    },
    generateReportPdf: function(req, res) { //Generate PDF
        try {

            // content of html 
            //	var html = fs.readFileSync('./test/businesscard.html', 'utf8');
            con.connect(function(err) {
                var sql = "SELECT * FROM simulationdata o, simulations i, user u  WHERE o.inputid=i.id and o.user = u.username and i.id = '" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);


                    if (result) {
                        result[0].data = JSON.parse(result[0].data);
                        result[0].simulation_data = JSON.parse(result[0].simulation_data);

                        var pbpkCat = '';
                        if (result[0].trial_name == 'mini-pbpk-trial') {
                            pbpkCat = 'Mini-PBPK';
                        } else if (result[0].trial_name == 'full-pbpk-trial') {
                            pbpkCat = 'Full-PBPK';
                        } else {
                            pbpkCat = 'VBE';
                        }

                        var html = '';
                        html = html.concat('<html><body style="font-weight: 100"><header style="margin-top:50px;"><div class="container" style="width:80%;float:none;margin:0 auto;"><div class="header-main" style="background:#ea4e4e;height:120px;width:100%;display:block;padding:10px 10px 50px 0px;font-weight:bold;"><div class="logo" style="display:inline-block;"><img src="http://simulation.theaxontech.com/assets/pdf/logo.png" style="margin-top: -45px;margin-left: -44px;width: 250px;"></div>');
                        html = html.concat('<div font-size:10px!importantclass="header-content" style="text-align:right;float:right!important;inline-block;font-weight: 100;font-size:10px!important"><h1 style="margin-bottom:10px;font-weight:bold;">SIMULATION REPORT</h1><h4 style="margin-bottom:0px;font-weight:bold;;"><a href="WWW.B2OSIMULATION.COM" style="color:#0563c1">WWW.B2OSIMULATION.COM</a></h4>');
                        html = html.concat('<h4 style="margin-top:5px;font-weight:bold;">' + formatDate(result[0].created_on) + ', ' + result[0].firstname + ' ' + result[0].lastname + '</h4></div></div>');
                        html = html.concat('<div class="pdf-content" style="width:100%;display:flex;justify-content: space-between;margin-top:80px;font-size:10px!important;"><div class="pdf-left-content" style=""><div class="one" style="margin-bottom:15px;"><h3 style="border-bottom:12px solid #ea4e4e;margin-bottom:10px;float-left">INPUT</h3>');
                        html = html.concat('<h4 style="font-weight:100;">' + result[0].type + ', ' + result[0].simulation_data.population.type + ', ADAM, ' + pbpkCat + ', Clearance, SD</h4></div>');


                        html = html.concat('<div><br><h3 style="border-bottom:10px solid #ea4e4e;margin-bottom:15px;float-left">SIGNATURE</h3><h4 style="font-weight:100;">Modeler Name:<input type="text" style="width:59%;outline:none!important;border-bottom: 1px solid #000!important;border: none;font-weight:100;"/></h4><h4 style="font-weight:100;">Signature:<input type="text" style="width:72%;outline:none!important;border-bottom: 1px solid #000!important;border: none;font-weight:100;"/></h4><h4 style="font-weight:100;">Date:<input type="text" style="width:85%;outline:none!important;border-bottom: 1px solid #000!important;border: none;font-weight:100;"/></h4></div></div>');

                        html = html.concat('<div class="one" style=""><h3 style="border-bottom:15px solid #ea4e4e;margin-bottom:15px;float-left;">SIMULATION INPUT</h3><h3 style="">POPULATION - ' + result[0].simulation_data.population.type + '</h3>');
                        html = html.concat('<div class="pdf-right-content" style="margin-left:10px;"><table style="width:80%;"><tr style="background-color: #dddddd;font-size:8px!important;"><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;font-size:8px!important;">' + result[0].simulation_data.population.type + '</th><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;font-size:8px!important;"></th></tr>');
                        // pulation table 
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">blood flow</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                        for (var key in result[0].simulation_data.population.subject.blood_flow) {
                            html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.population.subject.blood_flow[key] + '</td></tr>');
                        }
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">tissue volume</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                        for (var key in result[0].simulation_data.population.subject.tissue_volume) {
                            html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.population.subject.tissue_volume[key] + '</td></tr>');
                        }
                        html = html.concat('</table>');

                        //drug data 
                        html = html.concat('<table style="width:100%;"><tr style="background-color: #dddddd;font-size:8px!important;"><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.name + '</th><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></th></tr>');
                        html = html.concat('<h2 style="">DRUG - ' + result[0].simulation_data.drug.name + '</h2>');
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">physchem</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');

                        for (var key in result[0].simulation_data.drug.physchem) {
                            html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.physchem[key] + '</td></tr>');
                        }
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">absorption</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');

                        if (result[0].simulation_data.drug.absorption.adam) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">ADAM</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">bioavailability</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.bioavailability + '</td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">ka</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.ka + '</td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">fa</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.fa + '</td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">papp caco</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.papp_caco + '</td></tr>');


                            for (var i = 0; i < result[0].simulation_data.drug.absorption.adam.dissolution.length; i++) {
                                html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"><b>Time</b></td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"><b>' + result[0].simulation_data.drug.absorption.adam.dissolution[i].time + '</b></td></tr>');
                                html = html.concat('<tr  style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Test</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Reference</td></tr>');
                                html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.dissolution[i].test + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.dissolution[i].reference + '</td></tr>');
                            }


                        } else if (result[0].simulation_data.drug.absorption.dlm) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">DLM</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Release</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Release Time</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Release Fraction</td></tr>');
                            if (result[0].simulation_data.drug.absorption.adam != undefined) { //9/6/2018 change
                                for (var i = 0; i < result[0].simulation_data.drug.absorption.adam.particle.length; i++) {
                                    html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.particle[i].particle_radius + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.particle[i].particle_fraction + '</td></tr>');
                                }
                                html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">dissolution</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                                html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">particle radius</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">particle fraction</td></tr>');

                                for (var i = 0; i < result[0].simulation_data.drug.absorption.adam.dissolution.length; i++) {
                                    html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.dissolution[i].particle_radius + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.adam.dissolution[i].particle_fraction + '</td></tr>');
                                }

                            }


                        } else if (result[0].simulation_data.drug.absorption.first_order) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">First Order</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.absorption.first_order) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.absorption.first_order[key] + '</td></tr>');
                            }
                        }

                        //metabolism
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">metabolism</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');

                        if (result[0].simulation_data.drug.metabolism.vbe) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">vbe</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.metabolism.vbe) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.metabolism.vbe[key] + '</td></tr>');
                            }

                        } else if (result[0].simulation_data.drug.metabolism.mini_pbpk) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">mini pbpk</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.metabolism.mini_pbpk) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.metabolism.mini_pbpk[key] + '</td></tr>');
                            }
                        } else if (result[0].simulation_data.drug.metabolism.full_pbpk) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">full pbpk</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.metabolism.full_pbpk) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.metabolism.full_pbpk[key] + '</td></tr>');
                            }
                        }


                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">distribution</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');

                        //distribution
                        if (result[0].simulation_data.drug.distribution.vbe) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">vbe</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.distribution.vbe) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.distribution.vbe[key] + '</td></tr>');
                            }
                        } else if (result[0].simulation_data.drug.distribution.mini_pbpk) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">mini pbpk</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.distribution.mini_pbpk) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.distribution.mini_pbpk[key] + '</td></tr>');
                            }
                        } else if (result[0].simulation_data.drug.distribution.full_pbpk) {
                            html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">full pbpk</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                            for (var key in result[0].simulation_data.drug.distribution.full_pbpk) {
                                html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + result[0].simulation_data.drug.distribution.full_pbpk[key] + '</td></tr>');
                            }
                        }
                        html = html.concat('</table>');

                        var trial = {};
                        if (result[0].simulation_data.trial_design.vbe) {
                            trial = result[0].simulation_data.trial_design.vbe;
                        } else if (result[0].simulation_data.trial_design.full_pbpk) {
                            trial = result[0].simulation_data.trial_design.full_pbpk;
                        } else if (result[0].simulation_data.trial_design.mini_pbpk) {
                            trial = result[0].simulation_data.trial_design.mini_pbpk;
                        }

                        html = html.concat('<h3 style="">Trial Design - ' + result[0].trial_name + '</h3>');
                        html = html.concat('<div class="pdf-right-content" style="width:69%;"><table style="width:100%;"><tr style="background-color: #dddddd;font-size:8px!important;"><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Subject</th><th style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></th></tr>');

                        for (var key in trial.subject) {
                            html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + trial.subject[key] + '</td></tr>');
                        }
                        html = html.concat('<tr style="font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;font-size:8px!important;">Plan</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;"></td></tr>');
                        for (var key in trial.plan) {
                            html = html.concat('<tr style="background-color: #dddddd;font-size:8px!important;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + key + '</td><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">' + trial.plan[key] + '</td></tr>');
                        }
                        html = html.concat('</table></div></div></div></div>');

                        html = html.concat('<div  style="width:100%;font-size:12px!important;"><h3 style="border-bottom:12px solid #ea4e4e;margin-bottom:15px;">Output</h3>');


                        //graphs 
                        for (var i = 0; i < req.body.images.length; i++) {
                            html = html.concat('<h3 style="border-bottom:12px solid #ea4e4e;margin-bottom:15px;">' + req.body.images[i].name + '</h3><h2 style="margin-bottom:20px">');
                            html = html.concat('<img width="100px" height="200px" src="' + req.body.images[i].image + '" style="width:100%"></h2>');
                        }


                        // simulation output

                        html = html.concat('<div class="one" style="margin-bottom:20px;"><h3 style="border-bottom:12px solid #ea4e4e;margin-bottom:15px;font-size:12px!important;">PK PARAMETERS</h3><h4 style="font-weight:100;font-size:10px!important;">PK PARAMETER SUMMARY</h4>');
                        html = html.concat('<h4 style="margin-bottom:0px;font-size:10px!important;font-weight:100;">(Please show in table, Cmax, AUC, Tmax, t1/2, including their statistics, mean, CV, 5th/95th percentile, max/min)</h4></div>');

                        html = html.concat('<div class="one" style="margin-bottom:20px;"></div></div></div></div></header></body></html>');
                    }

                    var options = {
                        "format": 'Letter',
                        "font-weight": "100",
                        "font-size": "10px",
                        "footer": {
                            "height": "28mm",
                            "font-size": "8px",
                            "contents": {
                                default: '<div class="pdf-social-icons" style="width:100%; float:left;font-size:8px!important;"><div class="social" style="width:25%;float:left;" ><div class="socila-img" style="width: 50%;text-align: center;float:left"><img src="http://simulation.theaxontech.com/assets/pdf/pdf-message.png" style="width: 30%;text-align: center;float: none;margin: 0 auto;"></div><div class="socila-img-name"style="width: 50%;"><h4 style="text-align:center;font-weight:100!important;">EMAIL</h4></div></div><div class="social" style="width:25%;float:left;" ><div class="socila-img" style="width: 30%;position: relative;text-align: center;float: none;margin: 0 auto;"><img src="http://simulation.theaxontech.com/assets/pdf/pdf-twitter.png" style="width: 30%;text-align: center;float: none;margin: 0 auto;"></div><div class="socila-img-name"><h4 style="text-align:center;font-weight:100!important;">TWITTER HANDLE</h4></div></div><div class="social" style="width:25%;float:left;" ><div class="socila-img" style="width: 30%;text-align: center;float: none;margin: 0 auto;"><img src="http://simulation.theaxontech.com/assets/pdf/pdf-talephone.png" style="width: 30%;text-align: center;float: none;margin: 0 auto;"></div><div class="socila-img-name"><h4 style="text-align:center;font-weight:100!important;">TELEPHONE</h4></div></div><div class="social" style="width:25%;float:left;" ><div class="socila-img" style="width: 30%;position: relative;text-align: center;float: none;margin: 0 auto;"><img src="http://simulation.theaxontech.com/assets/pdf/pdf-link-in.png" style="width: 30%;text-align: center;float: none;margin: 0 auto;"></div><div class="socila-img-name"><h4 style="text-align:center;font-weight:100!important;">LINKEDIN URL</h4></div></div></div>',
                            }
                        },
                        "header": {
                            "height": "10mm",
                            "contents": ''
                        }
                    };
                    pdf.create(html, options).toFile('./simulationResult.pdf', function(err, data) {
                        if (err) return console.log(err);
                        fs.readFile('./simulationResult.pdf', function(err, data) {
                            res.setHeader('Content-Length', '100mb');
                            res.setHeader('Content-Type', 'application/pdf');
                            res.send(data);
                        });
                    });
                });
            });

        } catch (err) {
            res.status(500).send(err);
        }


    },
    generateCSV: function(req, res) { //generate csv
        try {
            con.connect(function(err) {
                var sql = "SELECT * FROM simulationdata o, simulations i, user u WHERE o.inputid=i.id and o.user = u.username and i.id = '" + req.body.id + "'";
                con.query(sql, function(err, result) {
                    if (err) res.send(err);
                    if (result) {
                        res.send(result);
                    }
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    }
}

function saveSimulationData(req, res, data) {
    try {
        if (data == 'system error!') {
            res.send({
                'error': data
            });
        }

        var result = JSON.parse(data);

        var output = JSON.stringify(result.result);
        con.connect(function(err) {
            var sql = "INSERT INTO simulationdata (inputid, user, data, type) VALUES ('" + req.body.insertId + "','" + req.body.auth + "','" + output + "', '" + req.body.studyType + "')";
            con.query(sql, function(err, result) {
                if (err) {
                    res.send(err);

                } else {
                    res.send({
                        'outputId': result.insertId
                    });
                }
            });
        });
    } catch (err) {
        res.send(err);

    }
}

function formatDate(val) {
    try {
        if (val) {
            var date = new Date(val);
        } else {
            var date = new Date();
        }
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return yyyy + '-' + mm + '-' + dd;
    } catch (err) {
        res.status(500).send(err);
    }
}
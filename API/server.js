var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var mysql = require('mysql');
var app = express();
var server = http.createServer(app);

//config
var config = require('./config/simulation.config');

// middlewares
var cors = require('./middleware/cors');


server.listen(config.PORT);

app.use(cors);

app.set('server', server);

app.use(bodyParser.json({
    limit: '50mb'
}));

app.use(bodyParser.urlencoded({
     extended: true
    , limit: '20mb'
}));

app.use(function (req, res, next) {
    var _send = res.send;
    var sent = false;
    res.send = function (data) {
        if (sent) return;
        _send.bind(res)(data);
        sent = true;
    };
    next();
});

require('./routes/simulation.routes')(app);
require('./routes/admin.routes')(app);



